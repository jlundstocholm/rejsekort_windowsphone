﻿using NUnit.Framework;
using NUnit.Framework.Constraints;
using System;
using System.IO;
using RejsekortHttpComLib;

namespace RejsekortHttpComLibTest
{
	[TestFixture ()]
	public class Test
	{
        enum HtmlFile
        {
            frontpage_2cards,
            cardoverview1,
            cardoverview2,
            englishpage,
            cardhistory1,
            cardhistory2_long_first,
            cardhistory2_long_last,
            cardhistory_checkout,
            cardhistory_information_missing_last,
            cardhistory3,
            englishpage2
        }

		private string loadHtmlFile(HtmlFile htmlFile) {
			using(StreamReader reader = new StreamReader("../../HtmlTestPages/" + htmlFile + ".html"))
			{
				return reader.ReadToEnd();
			}
		}

		[Test]
		public void TestCardList2Cards()
		{
            var cards = Extractors.CardList(loadHtmlFile(HtmlFile.frontpage_2cards));

			Assert.That(cards.Count, Is.EqualTo(2));

            Assert.That(cards[0].ElectronicCardNo, Is.EqualTo("100848"));
            Assert.That(cards[1].ElectronicCardNo, Is.EqualTo("200506943"));

            Assert.That(cards[0].PublicCardNo, Is.EqualTo("308430 000 100 848 8"));
            Assert.That(cards[1].PublicCardNo, Is.EqualTo("308430 200 506 943 3"));

            Assert.That(cards[0].CardType, Is.EqualTo("Voksen - Rejsekort personligt uden foto"));
            Assert.That(cards[1].CardType, Is.EqualTo("Voksen - Rejsekort flex"));

            Assert.That(cards[0].CardHolder, Is.EqualTo("Casper Bang"));
            Assert.That(cards[1].CardHolder, Is.EqualTo("Casper Bang"));

            Assert.That(cards[0].Balance, Is.EqualTo(235.48f));
            Assert.That(cards[1].Balance, Is.EqualTo(100f));

		}

		[Test]
		public void TestAutomaticReload1()
		{
            var tuple = Extractors.AutomaticReload(loadHtmlFile(HtmlFile.cardoverview1));
            Assert.That(tuple.Item2, Is.EqualTo(600f));
            Assert.That(tuple.Item1, Is.EqualTo(50f));
		}

        [Test]
        public void TestAutomaticReload2()
        {
            var tuple = Extractors.AutomaticReload(loadHtmlFile(HtmlFile.cardoverview2));
            Assert.That(tuple.Item2, Is.EqualTo(0f));
            Assert.That(tuple.Item1, Is.EqualTo(0f));
        }

        /*
		[Test]
		public void TestManualReload()
		{
            bool manualReload = Extractors.ManualReload(loadHtmlFile(HtmlFile.cardoverview1));
            Assert.That(manualReload, Is.False);
		}

		[Test]
		public void TestManualReload2()
		{
            bool manualReload = Extractors.ManualReload(loadHtmlFile(HtmlFile.ManualReloadExample));
            Assert.That(manualReload, Is.True);
		}
  */      

        [Test]
        public void TestDiscountLevels1() 
        {
            var levels = Extractors.DiscountLevels(loadHtmlFile(HtmlFile.cardoverview1));
            Assert.That(levels.Item1, Is.EqualTo(4));
            Assert.That(levels.Item2, Is.EqualTo(0));
            Assert.That(levels.Item3, Is.EqualTo(1));
        }

		[Test]
		public void TestDiscountLevels2() 
		{
            var levels = Extractors.DiscountLevels(loadHtmlFile(HtmlFile.cardoverview2));
            Assert.That(levels.Item1, Is.EqualTo(0));
            Assert.That(levels.Item2, Is.EqualTo(0));
            Assert.That(levels.Item3, Is.EqualTo(0));
		}

		[Test]
		public void TestIsInEnglish()
		{
            Assert.That(Extractors.IsInEnglish(loadHtmlFile(HtmlFile.cardoverview1)), Is.False);
            Assert.That(Extractors.IsInEnglish(loadHtmlFile(HtmlFile.englishpage)), Is.True);
            Assert.That(Extractors.IsInEnglish(loadHtmlFile(HtmlFile.englishpage2)), Is.True);
		}

		[Test]
		public void TestHistory() 
		{
            var travels = Extractors.History(loadHtmlFile(HtmlFile.cardhistory1)).Item1;
			Assert.That(travels.Count, Is.EqualTo(8));

            Assert.That(travels[0].StartPlace.Name, Is.EqualTo("Linje : 602 Maglebjerg Alle"));
            Assert.That(travels[0].StartPlace.EventDateTime, Is.EqualTo(
                DateTime.ParseExact("23-11-2015 07:07", "dd-MM-yyyy HH:mm", 
                System.Globalization.NumberFormatInfo.InvariantInfo)));
            Assert.That(travels[0].EndPlace.Name, Is.EqualTo("Skovlunde St."));
            Assert.That(travels[0].EndPlace.EventDateTime, Is.EqualTo(
                DateTime.ParseExact("23-11-2015 08:49", "dd-MM-yyyy HH:mm", 
                System.Globalization.NumberFormatInfo.InvariantInfo)));
            Assert.That(travels[0].Price, Is.EqualTo(-5720f));
            Assert.That(travels[0].Balance, Is.EqualTo(23911));

            Assert.That(travels[4].StartPlace.Name, Is.EqualTo("Information mangler"));
            Assert.That(travels[4].StartPlace.EventDateTime, Is.EqualTo(
                DateTime.ParseExact("25-11-2015 14:13", "dd-MM-yyyy HH:mm", 
                System.Globalization.NumberFormatInfo.InvariantInfo)));
            Assert.That(travels[4].EndPlace.Name, Is.Null);
            Assert.That(travels[4].EndPlace.EventDateTime, Is.Null);
            Assert.That(travels[4].Price, Is.EqualTo(0));
            Assert.That(travels[4].Balance, Is.EqualTo(0));

            /*
			// Test for midnight
			Assert.That(travels[19].StartPlace.EventDateTime, Is.EqualTo(
				DateTime.ParseExact("2014-01-22 14:12", "yyyy-MM-dd HH:mm", 
					System.Globalization.NumberFormatInfo.InvariantInfo)));
			Assert.That(travels[19].EndPlace.EventDateTime, Is.EqualTo(
				DateTime.ParseExact("2014-01-23 01:51", "yyyy-MM-dd HH:mm", 
					System.Globalization.NumberFormatInfo.InvariantInfo)));
            */
		}


        [Test]
        public void TestHistory2LongFirst()
        {
            string pagenum = Extractors.History(loadHtmlFile(HtmlFile.cardhistory2_long_first)).Item2;

            Assert.That(pagenum, Is.Not.Null);
            Assert.That(pagenum, Is.EqualTo("6"));
        }

        [Test]
        public void TestHistory2LongLast()
        {
            string pagenum = Extractors.History(loadHtmlFile(HtmlFile.cardhistory2_long_last)).Item2;
            Assert.That(pagenum, Is.Null);
        }

        //[Test]
        //public void TestHistoryWithNulls()
        //{
        //    var travels = Extractors.History(loadHtmlFile(HtmlFile.cardhistory3)).Item1;
        //    Assert.That(travels.Count, Is.EqualTo(16));
        //    Assert.That(travels[10].StartPlace.EventDateTime, Is.Not.Null);
        //    Assert.That(travels[10].StartPlace.EventDateTime, Is.EqualTo(
        //        DateTime.ParseExact("2015-04-04 00:00", "yyyy-MM-dd HH:mm",
        //            System.Globalization.NumberFormatInfo.InvariantInfo)));
        //    Extractors.SortTravels(travels);
            

        //}

        [Test]
        public void TestHistoryLatestServerUpdate1()
        {
            var travels = Extractors.History(loadHtmlFile(HtmlFile.cardhistory_checkout)).Item1;
            Extractors.SortTravels(travels);
            var update = Extractors.getLatestServerUpdate(travels);
            Assert.That(update, Is.Not.Null);
            Assert.That(update, Is.EqualTo(
                DateTime.ParseExact("30-11-2015 14:13", "dd-MM-yyyy HH:mm", 
                    System.Globalization.NumberFormatInfo.InvariantInfo)));
        }

        [Test]
        public void TestHistoryLatestServerUpdate2()
        {
            var travels = Extractors.History(loadHtmlFile(HtmlFile.cardhistory_information_missing_last)).Item1;
            Extractors.SortTravels(travels);
            var update = Extractors.getLatestServerUpdate(travels);
            Assert.That(update, Is.Not.Null);
            Assert.That(update, Is.EqualTo(
                DateTime.ParseExact("27-11-2015 12:13", "dd-MM-yyyy HH:mm", 
                    System.Globalization.NumberFormatInfo.InvariantInfo)));
        }

        [Test]
        public void TestAntiForgeryToken()
        {
            var token = Extractors.ExtractAntiForgeryToken(loadHtmlFile(HtmlFile.cardhistory1));
            Assert.That(token, Is.EqualTo("fbdKNDEmi0TdDjzes3ibSrCe8n1HAxgEPQhDhm/0iimNv/DxdD9W8csseK2FGERmhHRENhsAS3FM3Vr8ERlPWO7n8UkWLdMdljt09BRLTu00FtPH2iGEGAQlwr30HlYolqYOvIBSnhVd1t2LAOcCnrpsFGo="));
        }
	}
}

