﻿using NUnit.Framework;
using System;
using RejsekortHttpComLib;

namespace RejsekortHttpComLibTest
{
    [TestFixture()]
    public class TestCom
    {
        //Tests that is using network will only work on xamarin. Not VS
        //[Test()]
        public void TestDoLogin()
        {
            var lib = new RejsekortHttpComLib.ServerCom();
            var result = lib.DoLogin("jaybora", "Lotte1601");
            result.Wait();

            Console.WriteLine("Login result was {0}", result.Result);
            Assert.That(result.Result, Is.True);
        }

        //[Test()]
        [ExpectedException(typeof(AggregateException))]
        public void TestDoLoginBadCredentials()
        {
            var lib = new RejsekortHttpComLib.ServerCom();
            var result = lib.DoLogin("jay", "L");
            result.Wait();

        }


        //[Test()]
        public void TestFetchData()
        {
            var c = new Com();
            var progress = new Progress<ComProgress>();
            progress.ProgressChanged += (object sender, ComProgress e) => {
                Console.WriteLine("Progress update: {0}", e);
            };
            var cardListTask = c.FetchData("jaybora", "Lotte1601", progress, ServerCom.HistoryPeriod.ThreeMonths);
            Console.WriteLine("Awaiting task to finish...");
            cardListTask.Wait();
            var cards = cardListTask.Result;
            Assert.That(cards.Count, Is.EqualTo(1));
        }

        //[Test()]
        public void TestFetchCardList()
        {
            var lib = new RejsekortHttpComLib.ServerCom();
            lib.DoLogin("jaybora", "Lotte1601").Wait();
            var cardListTask = lib.FetchCardList();
            cardListTask.Wait();
            Assert.That(cardListTask.Result.Length, Is.AtLeast(100));
        }

        //[Test()]
        public void TestFetchHistory()
        {
            var lib = new RejsekortHttpComLib.ServerCom();
            lib.DoLogin("jaybora", "Lotte1601").Wait();
            var historyHtml = lib.FetchHistory(ServerCom.HistoryPeriod.ThreeMonths, null, null);
            historyHtml.Wait();
            Assert.That(historyHtml.Result.Length, Is.AtLeast(100));
        }


    }
}

