﻿#define DEBUG_AGENT

using System.Diagnostics;
using System.Windows;
using Microsoft.Phone.Scheduler;
using Microsoft.Phone.Shell;
using System;
using System.IO.IsolatedStorage;
using System.Linq;


namespace MitRejsekortAgent
{
    public class ScheduledAgent : ScheduledTaskAgent
    {
        /// <remarks>
        /// ScheduledAgent constructor, initializes the UnhandledException handler
        /// </remarks>
        static ScheduledAgent()
        {
            // Subscribe to the managed exception handler
            Deployment.Current.Dispatcher.BeginInvoke(delegate
            {
                Application.Current.UnhandledException += UnhandledException;
            });
        }

        /// Code to execute on Unhandled Exceptions
        private static void UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                Debugger.Break();
            }
        }

        /// <summary>
        /// Agent that runs a scheduled task
        /// </summary>
        /// <param name="task">
        /// The invoked task
        /// </param>
        /// <remarks>
        /// This method is called when a periodic or resource intensive task is invoked
        /// </remarks>
        protected override void OnInvoke(ScheduledTask task)
        {
            try
            {
                var username = GetSetting("Username1");
                var password = GetSetting("Password1");
                var interval = GetSettingInt("UpdateInterval");
                var lastRun = GetSettingForDate("LastUpdate");
                if (username != null && password != null && RunNow(lastRun, interval))
                {
                    var updater = new MitRejsekortUpdater.Updater(username, password, null);
                    updater.CardUpdatedEvent += updater_CardUpdatedEvent;
                    updater.Update(false).Wait();
                    NotifyComplete();
                }

            }
            catch (System.Exception)
            {
                throw;
            }
        }

        void updater_CardUpdatedEvent(MitRejsekort.Storage.CardDataItem card, MitRejsekortUpdater.UpdatedData updatedEvent)
        {
            RejsekortTilesAndToasts.Toasts.CardUpdatedEvent(card, updatedEvent);
            RejsekortTilesAndToasts.Tiles.UpdateTile(card);
        }

        private bool RunNow(DateTime? lastRun, int? interval)
        {
            if (lastRun == null || interval == null || interval.Equals("0"))
            {
                return false;
            }
            else
            {
                switch (interval)
                {
                    case 6: // Half hour
                        return true;
                    case 5: // One hour
                        return CheckInterval(lastRun, 1);
                    case 4: // 6th hour
                        return CheckInterval(lastRun, 6);
                    case 3: // 12th hour
                        return CheckInterval(lastRun, 12);
                    case 2:
                        return CheckInterval(lastRun, 24);
                    case 1:
                        return CheckInterval(lastRun, 48);
                }  
                return false;
            }
        }

        private static bool CheckInterval(DateTime? lastRun, int hours)
        {
            var sinceLast = (DateTime.Now - lastRun.Value).Hours;
            if (sinceLast >= hours)
            {
                return true;
            }
            return false;
        }

        public static string GetSetting(string key)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                return IsolatedStorageSettings.ApplicationSettings[key] as string;
            }
            else
            {
                return null;
            }
        }

        public static DateTime? GetSettingForDate(string key)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                return IsolatedStorageSettings.ApplicationSettings[key] as DateTime?;
            }
            else
            {
                return null;
            }
        }

        public static int? GetSettingInt(string key)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                return IsolatedStorageSettings.ApplicationSettings[key] as int?;
            }
            else
            {
                return null;
            }
        }
    }
}