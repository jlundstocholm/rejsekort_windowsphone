﻿using Microsoft.Phone.Shell;
using MitRejsekort.Storage;
using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace RejsekortTilesAndToasts
{
    public class Tiles
    {


        public static void UpdateTile(CardDataItem card)
        {
            var tile = FindTile(card);
            // We are getting update events for cards that may not be on a tile, so just ignore them
            if (tile != null)
            {
                tile.Update(GetSecondaryTileData(card));
            }
        }

        public static FlipTileData GetSecondaryTileData(CardDataItem card)
        {
            var lowBalance = card.LowBalanceWarningVal > card.Balance.ToFloat();
            var tileData = new FlipTileData
            {
                Title = AppResources.ApplicationTitle,
                BackTitle = card.CardNameCalculated,
                WideBackgroundImage = new Uri("/Rejsekort-windowsphone-wide-tile-icon.png", UriKind.Relative),
                WideBackBackgroundImage = new Uri("/Rejsekort-flip-tile-wide.png", UriKind.Relative),
                SmallBackgroundImage = new Uri("/Rejsekort-flip-tile-small.png", UriKind.Relative),
                BackgroundImage = new Uri("/Rejsekort-flip-tile.png", UriKind.Relative), 
                BackContent = String.Format("{0:0.00} kr", card.Balance.ToFloat()) + (lowBalance ? "\n" + AppResources.TileLowBalance : ""),
                WideBackContent = String.Format(AppResources.TileBalance, card.Balance.ToFloat()) + 
                        (lowBalance ? "\n" + AppResources.TileLowBalance : "") + "\n" +
                        String.Format(AppResources.TileUpdated, card.LatestClientUpdate.FromUnixTime())
            };

            return tileData;
        }

        public static IconicTileData GetIconicTileData(CardDataItem card)
        {
            var tileData = new IconicTileData
            {
                Title = card.CardNameCalculated,
                IconImage = new Uri("/Rejsekort-tile-icon-big.png", UriKind.Relative),
                SmallIconImage = new Uri("/Rejsekort-tile-icon-small.png", UriKind.Relative),
                WideContent1 = AppResources.ApplicationTitle,
                WideContent2 = String.Format(AppResources.TileBalance, card.Balance.ToFloat()),
                WideContent3 = String.Format(AppResources.TileUpdated, card.LatestClientUpdate.FromUnixTime())
            };

            return tileData;
        }

        public static ShellTile FindTile(CardDataItem card)
        {
            string tileUri = GetTileUri(card);
            ShellTile shellTile = ShellTile.ActiveTiles.FirstOrDefault(
                tile => tile.NavigationUri.ToString().Contains(tileUri));

            return shellTile;
        }

        public static string GetTileUri(CardDataItem card)
        {
            string tileUri = string.Concat("/CardPage.xaml?cardId=", card.Id);
            return tileUri;
        }
    }
}
