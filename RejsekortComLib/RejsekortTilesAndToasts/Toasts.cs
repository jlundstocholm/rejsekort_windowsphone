﻿using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RejsekortTilesAndToasts
{
    public class Toasts
    {
        public static void CardUpdatedEvent(MitRejsekort.Storage.CardDataItem card, MitRejsekortUpdater.UpdatedData updatedEvent)
        {
            if ("Y".Equals(card.WarnUpdates) && updatedEvent == MitRejsekortUpdater.UpdatedData.NEW_DATA)
            {
                ShellToast toast = new ShellToast();
                toast.Title = AppResources.ApplicationTitle;

                toast.Content = String.Format(AppResources.ToastNewData, card.CardNameCalculated);
                toast.Show();
            }
            if ("Y".Equals(card.WarnLowBalance) && updatedEvent == MitRejsekortUpdater.UpdatedData.LOW_BALANCE)
            {
                ShellToast toast = new ShellToast();
                toast.Title = AppResources.ApplicationTitle;
                // TODO CardName may be empty
                toast.Content = String.Format(AppResources.ToastLowBalance, card.CardNameCalculated);
                toast.Show();
            }
            if ("Y".Equals(card.WarnReload) && updatedEvent == MitRejsekortUpdater.UpdatedData.RELOAD)
            {
                ShellToast toast = new ShellToast();
                toast.Title = AppResources.ApplicationTitle;
                toast.Content = String.Format(AppResources.ToastReload, card.CardNameCalculated);
                toast.Show();
            }
        }


    }
}
