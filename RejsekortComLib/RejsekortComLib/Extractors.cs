﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Collections;
using System.Net;

namespace RejsekortHttpComLib
{
	public class Extractors
	{
        private const string ANTI_FORGERY_TOKEN = "var antiForgeryToken = '<input name=\"__RequestVerificationToken\" type=\"hidden\" value=\"(.*?)\" />";
        private const string SECOND_FRONTPAGE_OVERVIEW = "<div class=\"my_rejsekort_card\"(.*?)<div class=\"clear\">";
        private const string PUBLICCARDNO_REGEX = "<a href=\"/CWS/CardServices/Overview\\?cardElectronicNumber=(.*?)\">(.*?)</a>";
        private const string CARDTYPE_REGEX = "<div>(.*?)</div>";
        private const string CARDHOLDER_REGEX = "<div class=\"customer_name\">(.*?)</div>";
        private const string BALANCE_REGEX = "<span class=\"bold right\">(.*?)</span>";
        private const string DISCOUNTLEVEL_REGEX = "<span class=\"levelIntOverview\">(.*?)</span>";
        private const string AUTOMATICRELOAD_AMOUNT_REGEX = "<div align=\"left\">Tank-op-beløb <b> kr. (.*?)</b>";
        private const string AUTOMATICRELOAD_BALANCE_REGEX = "<div align=\"left\">Tank-op ved saldo <b> kr. (.*?)</b>";

        private const string HISTORY_REGEX_OVERVIEW = "<table class=\"tblJM\" id=\"historyTravels\" cellpadding=\"0\" cellspacing=\"0\" style=\"padding-top: 0.5em;\">(.*?)</table>";
        private const string HISTORY_REGEX_ROW = "<tr (.*?)class=\"trJL1\">(.*?)</tr>";
        private const string HISTORY_REGEX_FIELD = "<td(.*?)>(.*?)</td>";
        private const string HISTORY_REGEX_PAGE = "\"paginationButton\" data-pageNumber=\"([0-9]*)\" name=\"submitPage\">N";

        //    private const string HISTORY_REGEX_3 = "(-|[0-9]|,)*";
        private const string LANGUAGE_REGEX = "name=\"changeLanguageLink\">Dansk</a>";
        public const string SERVER_DATE_FORMAT = "dd-MM-yy HH:mm";


		private static string RegExMatch(string regex, string html, int matchGroup, RegexOptions options)
		{
			var RegEx = new Regex(regex, options);
			var match = RegEx.Match(html);
			if (match.Success)
			{
				return match.Groups[matchGroup].Value.Trim();
			}
			return null;
		}

		private static double toFloat(string danishFloat)
		{
            if (danishFloat == null)
            { 
                return 0f;
            }
			return double.Parse(danishFloat.Replace(".", "").Replace(",","."), 
				System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.AllowLeadingSign, 
				System.Globalization.NumberFormatInfo.InvariantInfo);
		}

		private static string RemoveHTML(string strHTML)
		{
            return WebUtility.HtmlDecode(Regex.Replace(strHTML, "<(.|\n)*?>", ""));
		}

		private static DateTime toDatetime(string serverDateString) 
		{
			try
			{
				return DateTime.ParseExact(serverDateString, SERVER_DATE_FORMAT, 
					System.Globalization.NumberFormatInfo.InvariantInfo);
			}
			catch (System.FormatException)
			{
				throw new ExtractException(serverDateString + " could not be parsed as a server date");
			}
		}

		public static List<CardInfo> CardList(string html)
		{
			var cards = new List<CardInfo>();
			var regex = new Regex (SECOND_FRONTPAGE_OVERVIEW, RegexOptions.Singleline);
			var match = regex.Match (html);
			// Iterate over the listed cards
			if (!match.Success)
			{
				throw new ExtractException("No cards found on server");
			}

			while (match.Success) 
			{
				var divPart = match.Groups[1].Value;
				var card = new CardInfo();

				// Match electroniccardno
                card.ElectronicCardNo = RegExMatch(PUBLICCARDNO_REGEX, divPart, 1, RegexOptions.Singleline);

				// Match publiccardno
				card.PublicCardNo = RegExMatch(PUBLICCARDNO_REGEX, divPart, 2, RegexOptions.Singleline);

				// Match cardtype
                card.CardType = CardType(divPart);

                // Match cardholder
                card.CardHolder = RemoveHTML(RegExMatch(CARDHOLDER_REGEX, divPart, 1, RegexOptions.Singleline));

                // Match balance
                card.Balance = (float)toFloat(RegExMatch(BALANCE_REGEX, divPart, 1, RegexOptions.None));

				cards.Add(card);
				match = match.NextMatch ();
			}
			return cards;
		}

        private static string CardType(string html) 
        {
            var regex = new Regex (CARDTYPE_REGEX, RegexOptions.Singleline);
            var match = regex.Match(html);
            while (match.Success)
            {
                var cardType = RemoveHTML((string)(match.Groups[1].Value).Trim());
                if (!cardType.Substring(0, 10).Equals("Kortnummer"))
                {
                    return cardType;
                }
                match = match.NextMatch ();
            }
            throw new ExtractException("CardType not found");
        }

            
        /*
         * Item1 is AutomaticReloadAt
         * Item2 is AutomaticReloadAmount
         */
        public static Tuple<float, float> AutomaticReload(string html)
		{
            var ret = new Tuple<float, float>(
                (float)toFloat(RegExMatch(AUTOMATICRELOAD_BALANCE_REGEX, html, 1, RegexOptions.Singleline)),
                (float)toFloat(RegExMatch(AUTOMATICRELOAD_AMOUNT_REGEX, html, 1, RegexOptions.Singleline)));
            return ret;
		}

        public static bool ManualReload(string html)
		{
            return false;
				//new Regex(MANUAL_RELOAD_PENDING_REGEX, RegexOptions.Singleline).Match(html).Success;
		}

        /*
         * Get discount levels as turple
         * Item1 is East
         * Item2 is West
         * Item3 is GreatBelt
         */
        public static Tuple<int, int, int> DiscountLevels(string html)
		{
            var regex = new Regex(DISCOUNTLEVEL_REGEX);
			var matcher = regex.Match(html);
			var i = 0;
            int east = 0;
            int west = 0;
            int greatBelt = 0;
			while (matcher.Success)
			{
				switch (i)
				{
					case 0:
                        east = int.Parse(matcher.Groups[1].Value);
						break;
					case 1:
                        west = int.Parse(matcher.Groups[1].Value);
						break;
					case 2:
                        greatBelt = int.Parse(matcher.Groups[1].Value);
						break;
				}
				i++;
				matcher = matcher.NextMatch();
			}
            return new Tuple<int, int, int>(east, west, greatBelt);
		}

		public static bool IsInEnglish(string html)
		{
			var regex = new Regex(LANGUAGE_REGEX);
			return regex.Match(html).Success;
		}

        public static void SortTravels(List<CardInfo.Travel> travels)
        {
            travels.Sort((x, y) => x.StartPlace.EventDateTime.Value.CompareTo(y.StartPlace.EventDateTime.Value) * -1);
        }

/*
        public static DateTime ServerUpdate(string html)
		{
			var updateAsString = RegExMatch(SERVER_UPDATE_REGEX, html, 1, RegexOptions.None);
			if (updateAsString == null)
			{
				throw new ExtractException("Server update is null");
			}
            return toDatetime(updateAsString);
		}
*/


        public static Tuple<List<CardInfo.Travel>, string> History(string html)
		{
			var regexOuterLoop = new Regex(HISTORY_REGEX_OVERVIEW, RegexOptions.Singleline);
			var travels = new List<CardInfo.Travel>();
			var matchOuterLoop = regexOuterLoop.Match(html);
            string nextPage = RegExMatch(HISTORY_REGEX_PAGE, html, 1, RegexOptions.Singleline);

			while (matchOuterLoop.Success)
			{
                string tablePart = (matchOuterLoop.Groups[1].Value).Trim();
                var regexRowLoop = new Regex(HISTORY_REGEX_ROW, RegexOptions.Singleline);
				var matchRow = regexRowLoop.Match(tablePart);
				
				CardInfo.Travel prevTravel = null;
				
				while (matchRow.Success) {
                    string rowPart = matchRow.Groups[2].Value.Trim();

                    //Get next page number
                    var fields = ExtractHistoryFields(rowPart);
                    if (fields.Count > 1)
                    {
                        CardInfo.Travel travel;
                        DateTime? departure = null;
                        bool departureHasNoTime = (fields[3] == null || fields[3].Length < 4);
                        if (fields.Count <= 3 || fields[2] == null || fields[2].Length < 8) {
                            if (prevTravel != null) {
                                departure = prevTravel.StartPlace.EventDateTime;
                            }
                        } else {
                            departure = toDatetime(fields[2] + " " + (departureHasNoTime ? "00:00" : fields[3]));

                        }
                        DateTime? arrival = null;
                        bool arrivalHasNoTime = (fields[5] == null || fields[5].Length < 4);
                        if (fields.Count <= 5 || fields[2] == null || fields[2].Length < 8) {

                        } else {
                            arrival = toDatetime(fields[2] + " " + (arrivalHasNoTime ? "23:59" : fields[5]));
                            if (departure.HasValue && arrival.Value.CompareTo(departure.Value) < 0) {
                                arrival = arrival.Value.AddDays(1);
                            }
                        }
                        travel = new CardInfo.Travel
                        {
                            StartPlace = new CardInfo.Place
                            {
                                EventDateTime = departure, 
                                hasNoTime = departureHasNoTime,
                                Name = (fields.Count >= 4 ? fields[4] : null)
                            },
                            EndPlace = new CardInfo.Place
                            {
                                EventDateTime = arrival,
                                hasNoTime = arrivalHasNoTime,
                                Name = (fields.Count >= 6 ? fields[6] : null)
                            }
                        };
                        travel.Price = fields[7] == null ? 0 : (int) (toFloat(fields[7])*100f);
                        if (travel.EndPlace.EventDateTime != null && travel.Price > 0) {
                            travel.Price = travel.Price * -1;
                        }
                        travel.TravelNo = fields[1] != null && fields[1].Length > 0 ? int.Parse(fields[1]):0;
                        travel.Balance = fields[8] == null ? 0 : (int) (toFloat(fields[8])*100f);

                        // If departure is null, we have no idea of where to show the activity, so we dont add it
                        if (departure != null)
                        {
                            travels.Add(travel);
                        }
                        prevTravel = travel;
                    }

					matchRow = matchRow.NextMatch();
				}
                matchOuterLoop = matchOuterLoop.NextMatch();
			}
            return new Tuple<List<CardInfo.Travel>, string>(travels, nextPage);
		}

        private static List<string> ExtractHistoryFields(String rowPart) {
            var regex = new Regex(HISTORY_REGEX_FIELD, RegexOptions.Singleline);
            var match = regex.Match(rowPart);


            var fields = new List<String>();
            while (match.Success) {
                String field = RemoveHTML(match.Groups[2].Value).Trim();
                if (field.Length == 0) {
                    field = null;
                }
                fields.Add(field);
                match = match.NextMatch();
            }
            return fields;
        }

        public static DateTime? getLatestServerUpdate(List<CardInfo.Travel> travels) 
        {
            for (int i = 0; i < travels.Count; i++) {
                var travel = travels[i];

                //Look in endplace first
                if (travel.EndPlace.EventDateTime.HasValue && !travel.EndPlace.hasNoTime) {
                    return travel.EndPlace.EventDateTime.Value;
                }

                //Then look in startplace
                if (travel.StartPlace.EventDateTime.HasValue) {
                    return travel.StartPlace.EventDateTime;
                }
            }
            return null;
        }

        public static string ExtractAntiForgeryToken(string content)
        {
            return RegExMatch(ANTI_FORGERY_TOKEN, content, 1, RegexOptions.Singleline);
        }
    }

	public class ExtractException : Exception
	{	
		public ExtractException (string msg) : base ( msg ){}
		public ExtractException (string msg, Exception innerException) : base ( msg, innerException ){}

	}

    public class LoginTokenException : Exception
    {
		public LoginTokenException (string msg) : base ( msg ){}
		public LoginTokenException (string msg, Exception innerException) : base ( msg, innerException ){}
        
    }
}

