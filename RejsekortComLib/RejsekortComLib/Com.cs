using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using WinPhoneExtensions;


namespace RejsekortHttpComLib
{

	public class Com
	{
        private ServerCom serverCom;
        const int MAX_ERRORS = 5;
        private string traceHtml {get; set;}
        private string traceUrl { get; set; }
        /**
         * Fetch all data from server that is available from the given credentials
         */

        public async Task<List<CardInfo>> FetchData(string username, string password, 
            IProgress<ComProgress> progress, ServerCom.HistoryPeriod historyPeriod)
        {
            serverCom = new ServerCom();
            // Do login
            if (progress != null)
            {
                progress.Report(new ComProgress {StepName = ComProgressStepName.LOGIN,
                    CurrentStep = 1, TotalSteps = 4});
            }
            await serverCom.DoLogin(username, password).AsyncTrace();
            Exception lastException = null;


            // Try a couple of times
            for (int errorcount = 0; errorcount < MAX_ERRORS; errorcount++)
            {
                try
                {
                    // Get cards
                    if (progress != null)
                    {
                        progress.Report(new ComProgress
                        {
                            StepName = ComProgressStepName.CARDLIST,
                            CurrentStep = 2,
                            TotalSteps = 4
                        });
                    }
                    var cards = await CardList().AsyncTrace<List<CardInfo>>();
                    cards = await CardData(cards, progress, historyPeriod).AsyncTrace<List<CardInfo>>();
                    return cards;
                }
                catch (Exception e)
                {
                    lastException = e;
                    lastException.Data.Add("html", traceHtml);
                    lastException.Data.Add("url", traceUrl);
                }
            }
            throw lastException;
        }
        private async Task<List<CardInfo>> CardList()
        {
            var html = await serverCom.FetchCardList().AsyncTrace<string>();
            if (Extractors.IsInEnglish(html))
            {
                serverCom.ChangeLanguage();
                html = await serverCom.FetchCardList().AsyncTrace<string>();
            }
            traceHtml = html;
            traceUrl = "CardList";
            return Extractors.CardList(html);
        }

        private async Task<List<CardInfo>> CardData(List<CardInfo> cards, IProgress<ComProgress> progress, ServerCom.HistoryPeriod period)
        {
            int cardCount = 0;
            foreach (var card in cards)
            {
                cardCount++;
                if (progress != null)
                {
                    progress.Report(new ComProgress {StepName = ComProgressStepName.BASISDATA,
                        CurrentStep = 2*cardCount +1, TotalSteps = 2 * cards.Count +3, TotalCards = cards.Count, CurrentCard = card});
                }

                var html = await serverCom.FetchCardDetails(card).AsyncTrace<string>();
                traceHtml = html;
                traceUrl = "CardDetails";

                var levels = Extractors.DiscountLevels(html);
                card.DiscountLevelEast = levels.Item1;
                card.DiscountLevelWest = levels.Item2;
                card.DiscountLevelGreatBelt = levels.Item3;

                var autoReloads = Extractors.AutomaticReload(html);
                card.AutomaticReloadAt = autoReloads.Item1;
                card.AutomaticReloadAmount = autoReloads.Item2;

                card.ManualReloadPending = Extractors.ManualReload(html);
                card.LatestClientUpdate = DateTime.Now;

                // Get history for card
                if (progress != null)
                {
                    progress.Report(new ComProgress {StepName = ComProgressStepName.HISTORY,
                        CurrentStep = 2*cardCount +2, TotalSteps = 2 * cards.Count +3, TotalCards = cards.Count, CurrentCard = card});
                }
                var travels = await CardHistory(period, null, null).AsyncTrace();
                card.Travels = travels.Item1;
                Extractors.SortTravels(card.Travels);
                card.LatestServerUpdate = Extractors.getLatestServerUpdate(card.Travels);
                // Get history for card
                if (progress != null)
                {
                    progress.Report(new ComProgress {StepName = ComProgressStepName.DONE,
                        CurrentStep = 2*cardCount +3, TotalSteps = 2 * cards.Count +3, TotalCards = cards.Count, CurrentCard = card});
                }
            }
            return cards;
        }

        private async Task<Tuple<List<CardInfo.Travel>, string>> CardHistory(
            ServerCom.HistoryPeriod period, string nextpage, string antiForgeryToken)
        {
            var history = await serverCom.FetchHistory(period, nextpage, antiForgeryToken).AsyncTrace<string>();
            traceHtml = history;
            traceUrl = "CardHistory";
            var tupleHistory = Extractors.History(history);
            var travels = tupleHistory.Item1;
            var newAntiForgeryToken = Extractors.ExtractAntiForgeryToken(history);
            var newnextpage = tupleHistory.Item2;
            if (newnextpage != null)
            {
                var newtuple = await CardHistory(period, newnextpage, newAntiForgeryToken);
                travels.AddRange(newtuple.Item1);
            }
            return tupleHistory;
        }
    }

    public class ComProgress
    {
        public int TotalSteps { get; set;}
        public int CurrentStep { get; set; }
        public CardInfo CurrentCard { get; set; }
        public int TotalCards { get; set; }
        public ComProgressStepName StepName { get; set; }

        public override string ToString()
        {
            return string.Format("[ComProgress: TotalSteps={0}, CurrentStep={1}, TotalCards={3}, StepName={4}, CurrentCard={2}]", TotalSteps, CurrentStep, CurrentCard, TotalCards, StepName);
        }
        
    }

    public enum ComProgressStepName
    {
        LOGIN,
        CARDLIST,
        BASISDATA,
        HISTORY,
        DONE
    }
}
