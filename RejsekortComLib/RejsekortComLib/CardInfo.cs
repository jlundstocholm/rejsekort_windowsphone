﻿using System;
using System.Collections.Generic;

namespace RejsekortHttpComLib
{
	public class CardInfo
	{
		public string CardHolder { get; set; }
		public float Balance { get; set; }
		public int DiscountLevelEast { get; set; }
		public int DiscountLevelWest { get; set; }
		public int DiscountLevelGreatBelt { get; set; }
        public List<Travel> Travels;
		public DateTime? LatestClientUpdate { get; set; }
		public DateTime? LatestServerUpdate { get; set; }
		public string PublicCardNo { get; set; }
		public string ElectronicCardNo { get; set; }
		public string CardType { get; set; }
		public bool ManualReloadPending { get; set; }
		public float AutomaticReloadAmount { get; set; }
		public float AutomaticReloadAt { get; set; }

        public override string ToString()
        {
            return string.Format("[CardInfo: CardHolder={0}, Balance={1}, DiscountLevelEast={2}, DiscountLevelWest={3}, DiscountLevelGreatBelt={4}, LatestClientUpdate={5}, LatestServerUpdate={6}, PublicCardNo={7}, ElectronicCardNo={8}, CardType={9}, ManualReloadPending={10}, AutomaticReloadAmount={11}, AutomaticReloadAt={12}]", CardHolder, Balance, DiscountLevelEast, DiscountLevelWest, DiscountLevelGreatBelt, LatestClientUpdate, LatestServerUpdate, PublicCardNo, ElectronicCardNo, CardType, ManualReloadPending, AutomaticReloadAmount, AutomaticReloadAt);
        }

        public class Travel
        {
            public Place StartPlace { get; set; }
            public Place EndPlace { get; set; }
            public int Price { get; set; }
            public int TravelNo { get; set; }
            public int Balance { get; set; }


            public override string ToString()
            {
                return string.Format("[Travel: StartPlace={0}, EndPlace={1}, Price={2}]", StartPlace, EndPlace, Price);
            }
            
        }

        public class Place
        {
            public DateTime? EventDateTime { get; set; }
            public bool hasNoTime { get; set; }
            public string Name { get; set; }

            public override string ToString()
            {
                return string.Format("[Place: EventDateTime={0}, Name={1}]", EventDateTime, Name);
            }
            
        }
	}

   
}
