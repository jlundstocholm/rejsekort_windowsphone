﻿using System;
using System.Net;
using System.IO;
using System.Threading;
using WinPhoneExtensions;
using System.Threading.Tasks;
using System.Text;
using System.Collections.Generic; 

namespace RejsekortHttpComLib
{
    public class ServerCom
    {
        private const string LOGIN_FORM_GET_URL = "https://selvbetjening.rejsekort.dk/CWS/Home/UserNameLogin";
        private const string LOGIN_FORM_POST_URL = "https://selvbetjening.rejsekort.dk/CWS/?isUsernameTab=True";
        private const string TRAVEL_HISTORY_URL = "https://selvbetjening.rejsekort.dk/CWS/TransactionServices/TravelCardHistory";
        private const string CHANGE_LANGUAGE_URL = "https://selvbetjening.rejsekort.dk/CWS/Home/ChangePageLanguage?a=TravelCardHistory&c=TransactionServices";
        private const string FRONTPAGE_URL = "https://selvbetjening.rejsekort.dk/CWS/CardServices/MyRejsekort";
        private const string CARDOVERVIEW_URL = "https://selvbetjening.rejsekort.dk/CWS/CardServices/Overview?cardElectronicNumber=";
        private const string USERNAME_HELP_URL = "https://selvbetjening.rejsekort.dk/CWS/Home/ForgotUsernamePassword";


        //private const int TIMEOUT_MILISECS = 20000;

        private bool isLoggedIn = false;
        public bool IsLoggedIn { get { return isLoggedIn; } }

        private CookieContainer cookieContainer = new CookieContainer();

        private StringBuilder traceHtml { get; set; }

        public enum HistoryPeriod
        {
            OneWeek,
            TwoWeeks,
            OneMonth,
            ThreeMonths,
            All
        }

        private async Task<string> FetchAsGet(string url)
        {
            var req = (HttpWebRequest)WebRequest.Create(url);
            req.CookieContainer = this.cookieContainer;
            using (var response = await req.GetResponseAsync().AsyncTrace<HttpWebResponse>())
            {
                if (response == null || response.GetResponseStream() == null)
                {
                    throw new Exception("Fetch from " + url + " returned nothing");
                }
                StreamReader streamRead = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8);
                //return PCLWebUtility.WebUtility.HtmlDecode(streamRead.ReadToEnd());
                return streamRead.ReadToEnd();
            }
        }

        private async Task<HttpWebResponse> FetchAsPost(string url, params KeyValuePair<string, string>[] postValues)
        {
            // Build up post of login
            var req = (HttpWebRequest)WebRequest.Create(url);
            req.CookieContainer = cookieContainer;
            req.Method = HttpMethod.Post;
            req.ContentType = ContentType.UrlEncoded;
            using (Stream dataStream = await req.GetRequestStreamAsync().AsyncTrace())
            {
                var builder = new StringBuilder();
                bool first = true;
                foreach (var pair in postValues)
                {
                    if (first)
                    {
                        first = false;
                    }
                    else
                    {
                        builder.Append("&");
                    }
                    builder.AppendFormat("{0}={1}", pair.Key, Uri.EscapeDataString(pair.Value));
                }
                var encode = Encoding.UTF8.GetBytes(builder.ToString());
                dataStream.Write(encode, 0, encode.Length);
            }
            return await req.GetResponseAsync().AsyncTrace();
        }


        public async Task<string> FetchAntiForgeryToken(string url)
        {
            return Extractors.ExtractAntiForgeryToken(await FetchAsGet(url).AsyncTrace());
        }

        /**
         * Do a login, and return true if success or throw LoginException.
         * The login cookie is stored in the instance, so keep through the communication process
         */

        public async Task<bool> DoLogin(string username, string password)
        {
            var token = await FetchAntiForgeryToken(LOGIN_FORM_GET_URL);
            using (var response = await FetchAsPost(LOGIN_FORM_POST_URL, 
                new KeyValuePair<string, string>("Username", username),
                new KeyValuePair<string, string>("Password", password),
                new KeyValuePair<string, string>("__RequestVerificationToken", token)
                ))
            {
                var replyUri = response.ResponseUri;
                if (replyUri != null && replyUri.ToString().Contains("MyRejsekort"))
                {
                    this.isLoggedIn = true;
                }
                else
                {
                    this.isLoggedIn = false;
                    throw new LoginException("Invalid username/password");
                }
                return this.isLoggedIn;
            }
        }

        /**
         * Return a list of available cards from rejsekort server as html
         */
        public async Task<string> FetchCardList()
        {
            return await FetchAsGet(FRONTPAGE_URL).AsyncTrace();
        }

        /**
         * Get the basis data from given card as html
         */

        public async Task<string> FetchCardDetails(CardInfo card)
        {
            return await FetchAsGet(CARDOVERVIEW_URL + card.ElectronicCardNo).AsyncTrace();
        }

        /**
         * Change language on the server
         */
        public async void ChangeLanguage()
        {
            var req = (HttpWebRequest)WebRequest.Create(CHANGE_LANGUAGE_URL);
            req.CookieContainer = cookieContainer;
            await req.GetResponseAsync().AsyncTrace();
        }

        /**
         * This will fetch the html with the full history of travels of the active card.
         * The active card is card that was last called by FetchCardDetails
         */
        public async Task<string> FetchHistory(HistoryPeriod period, string pageno, string antiForgeryToken)
        {
            if (antiForgeryToken == null)
            {
                antiForgeryToken = await FetchAntiForgeryToken(TRAVEL_HISTORY_URL);
            }
            
            if (pageno == null)
            {
                using (var response = await FetchAsPost(TRAVEL_HISTORY_URL,
                    new KeyValuePair<string, string>("periodSelected", period.ToString()),
                    new KeyValuePair<string, string>("__RequestVerificationToken", antiForgeryToken)).AsyncTrace())
                    {
                        StreamReader streamRead = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8);
                        return streamRead.ReadToEnd();
                    }
            }
            else
            {
                using (var response = await FetchAsPost(TRAVEL_HISTORY_URL,
                    new KeyValuePair<string, string>("periodSelected", period.ToString()),
                    new KeyValuePair<string, string>("page", pageno),
                    new KeyValuePair<string, string>("__RequestVerificationToken", antiForgeryToken)).AsyncTrace())
                {
                    StreamReader streamRead = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8);
                    return streamRead.ReadToEnd();
                }
            }
        }
    }

    public class LoginException : Exception
    {
        public LoginException()
        {
        }


        public LoginException(string message) : base(message)
        {
        }


        public LoginException(string message, Exception innerException) : base(message, innerException)
        {
        }

    }

}

