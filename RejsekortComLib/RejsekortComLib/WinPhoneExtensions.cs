﻿using System.Net;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.CompilerServices;
using System;
using System.Collections.Generic;

namespace WinPhoneExtensions 
{ 
    public static class HttpExtensions 
    { 
        public static Task<HttpWebResponse> GetResponseAsync(this HttpWebRequest request) 
        { 
            var taskComplete = new TaskCompletionSource<HttpWebResponse>();
            
            request.BeginGetResponse(asyncResponse => { 
                try
                {
                    HttpWebRequest responseRequest = (HttpWebRequest)asyncResponse.AsyncState;
                    HttpWebResponse someResponse = (HttpWebResponse)responseRequest.EndGetResponse(asyncResponse);
                    taskComplete.TrySetResult(someResponse);
                } catch (WebException webExc) {
                    HttpWebResponse failedResponse = (HttpWebResponse)webExc.Response;
                    taskComplete.TrySetResult(failedResponse); 
                } 
            }, request); 
            return taskComplete.Task; 
        } 

        public static Task<Stream> GetRequestStreamAsync(this HttpWebRequest request) 
        { 
            var taskComplete = new TaskCompletionSource<Stream>();
            request.AllowReadStreamBuffering = true;
            request.BeginGetRequestStream(asyncResponse => { 
                HttpWebRequest responseRequest = (HttpWebRequest)asyncResponse.AsyncState;
                Stream stream = (Stream)responseRequest.EndGetRequestStream(asyncResponse);
                taskComplete.TrySetResult(stream);
            }, request); 
            return taskComplete.Task; 
        } 

        /**
         * Read all received data and return as a string
         */
        public static string ReadToEnd(this HttpWebResponse response) 
        {
            StreamReader streamRead = new StreamReader(response.GetResponseStream());
            return streamRead.ReadToEnd();
        }

    }

    public static class HttpMethod 
    {
        public static string Head { get{return "HEAD";} } 
        public static string Post { get{return "POST";} } 
        public static string Put { get{return "PUT";} } 
        public static string Get { get{return "GET";} } 
        public static string Delete { get{return "DELETE";} }
        public static string Trace { get{return "TRACE";} } 
        public static string Options { get{return "OPTIONS";} }
        public static string Connect { get{return "CONNECT";} }
        public static string Patch { get{return "PATCH";} } 
    }

    public static class ContentType
    {
        public const string UrlEncoded = "application/x-www-form-urlencoded";
    }


    public static class AsyncExceptionExtensions
    {
        public static async Task AsyncTrace(
            this Task task,
            [CallerMemberName] string callerMemberName = null,
            [CallerFilePath] string callerFilePath = null,
            [CallerLineNumber] int callerLineNumber = 0)
        {
            try
            {
                await task;
            }
            catch (Exception ex)
            {
                AddAsyncStackTrace(ex, callerMemberName, callerFilePath, callerLineNumber);
                throw;
            }
        }

        public static async Task<T> AsyncTrace<T>(
            this Task<T> task,
            [CallerMemberName] string callerMemberName = null,
            [CallerFilePath] string callerFilePath = null,
            [CallerLineNumber] int callerLineNumber = 0)
        {
            try
            {
                return await task;
            }
            catch (Exception ex)
            {
                AddAsyncStackTrace(ex, callerMemberName, callerFilePath, callerLineNumber);
                throw;
            }
        }

        private static void AddAsyncStackTrace(Exception ex, string callerMemberName, string callerFilePath, int callerLineNumber = 0)
        {
            var trace = ex.Data["_AsyncStackTrace"] as IList<string>;
            if (trace == null)
                trace = new List<string>();
            trace.Add(string.Format("@{0}, in '{1}', line {2}", callerMemberName, callerFilePath, callerLineNumber));
            ex.Data["_AsyncStackTrace"] = trace;
        }

        public static string AsyncTrace(this Exception ex)
        {
            var trace = ex.Data["_AsyncStackTrace"] as IList<string>;
            if (trace == null)
                return string.Empty;
            return string.Join("\n", trace);
        }

        public static string FullTrace(this Exception ex)
        {
            var trace = ex.Data["_AsyncStackTrace"] as IList<string>;
            if (trace == null)
                return string.Empty;
            return string.Format("{0}\n--- Async stack trace:\n\t", ex)
                 + string.Join("\n\t", trace);
        }
    }
}