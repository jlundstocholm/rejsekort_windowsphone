﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Diagnostics;
using MitRejsekort.Storage;

namespace MitRejsekort
{
    public partial class CardPage : PhoneApplicationPage
    {
        public CardPage()
        {
            InitializeComponent();

            DataContext = App.ViewModel;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            GoogleAnalytics.EasyTracker.GetTracker().SendView("CardPage");
            if (NavigationContext.QueryString.ContainsKey("cardId"))
            {
                var cardId = NavigationContext.QueryString["cardId"].ToFloat();
                // Run this in a seperate thread to avoid deadlock on task waits
                Task.Run(() =>
                    {
                        var storage = RejsekortStorage.Instance().Result;
                        var historyItems = storage.GetActivitityHistoryItems((int)cardId).Result;
                        App.ViewModel.ActiveCard = new ViewModels.CardItemViewModel()
                        {
                            Card = storage.CardById(((int)cardId)).Result,
                            ActivityHistory = historyItems
                        };
                    }).Wait();
            }
        }

        private async void paiCardSettings_LostFocus(object sender, RoutedEventArgs e)
        {
            await App.ViewModel.SaveCardSetttings();
        }

        private async void saveCardSettings(object sender, RoutedEventArgs e)
        {
            await App.ViewModel.SaveCardSetttings();
        }


        private void txtLowBalance_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            // Comma should be ignored
            if (e.PlatformKeyCode == 188)
            {
                e.Handled = true;
            }
        }

    }
}