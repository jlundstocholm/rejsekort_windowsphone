﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using MitRejsekort.Resources;
using RejsekortHttpComLib;
using MitRejsekort.Storage;
using System.Windows;
using System.Threading.Tasks;
using System.Diagnostics;

namespace MitRejsekort.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            
            this.Cards = new ObservableCollection<CardItemViewModel>();
            this.Settings = new SettingsViewModel();
            this.FullVersion = false;


        }

        private bool _fullVersion;
        public bool FullVersion
        { 
            get
            {
                return _fullVersion;
            }
            set
            { 
                _fullVersion = value;
                NotifyPropertyChanged("TrailVersionVisibility");
                Debug.WriteLine("FullVersion flag changed to {0}", _fullVersion);
 
            }
        }

        private CardItemViewModel _activeCard;
        public CardItemViewModel ActiveCard
        {
            get
            {
                if (_activeCard == null && Cards.Count > 0)
                {
                    _activeCard = Cards[0];
                }
                return _activeCard;
            }
            set
            {
                _activeCard = value;
                NotifyPropertyChanged("ActiveCard");
            }
        }

        public ObservableCollection<CardItemViewModel> Cards { get; set; }
        public SettingsViewModel Settings { get; set; }
        public bool IsIndeterminate
        {
            get
            {
                return ComProgress != null;
            }
        }
        private bool _IsLoading;
        public bool IsLoading 
        { 
            get 
            {
                return _IsLoading;
            }
            set
            {
                _IsLoading = value;
                NotifyPropertyChanged("IsLoading");
                NotifyPropertyChanged("NoCardsVisibility");
            }
        }

        public Visibility ComProgressVisibility
        {
            get
            {
                return _ComProgress == null ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        public Visibility TrailVersionVisibility
        {
            get
            {
                return _fullVersion ? Visibility.Collapsed : Visibility.Visible;

            }
        }
 
        private ComProgress _ComProgress;
        public ComProgress ComProgress
        {
            get 
            { 
                return _ComProgress; 
            } 
            set 
            {
                _ComProgress = value;
                NotifyPropertyChanged("ComProgress");
                NotifyPropertyChanged("ComProgressVisibility");
                NotifyPropertyChanged("IsIndeterminate");
                
            }
        }


        public bool IsDataLoaded
        {
            get;
            private set;
        }

        public string NoCardsVisibility
        {
            get
            {
                if (IsLoading || Cards.Count > 0)
                {
                    return "Collapsed";
                }
                else
                {
                    return "Visible";
                }
            }
        }

        public bool AgentsAreEnabled { get; set; }

        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// </summary>
        public async Task LoadData()
        {
            IsLoading = true;
            await LoadCards();
            this.IsDataLoaded = true;
            IsLoading = false;
        }

        public async Task LoadCards()
        {
            var storage = await RejsekortStorage.Instance();
            var cards = await storage.Cards();
            Debug.WriteLine("Number of cards in database {0}", cards.Count);
            Cards.Clear();
            foreach (var card in cards)
            {
                var historyItems = await storage.GetActivitityHistoryItems(card.Id);

                if (!FullVersion)
                {
                    historyItems = historyItems.GetRange(0, Math.Min(5, historyItems.Count));
                    historyItems.Add(new ActivityHistoryItem()
                        {
                            StartPlace = AppResources.TrailVersion,
                            EndPlace = AppResources.TrailVersionExplanation
                        });
                }
                
                Cards.Add(new ViewModels.CardItemViewModel()
                {
                    Card = card,
                    ActivityHistory = historyItems

                });
            }
            NotifyPropertyChanged("Cards");
            NotifyPropertyChanged("NoCardsVisibility");
            if (Cards.Count == 0)
            {
                _activePivotItem = 1;
                NotifyPropertyChanged("ActivePivotItem");
            }
        }

        public async Task SaveCardSetttings()
        {
            var storage = await RejsekortStorage.Instance();
            await storage.UpdateCardNameAndStuff(ActiveCard.Card,
                ActiveCard.Card.CardName,
                ActiveCard.Card.LowBalanceWarningVal,
                "Y".Equals(ActiveCard.Card.InactiveCard),
                "Y".Equals(ActiveCard.Card.WarnLowBalance),
                "Y".Equals(ActiveCard.Card.WarnReload),
                "Y".Equals(ActiveCard.Card.WarnUpdates));
            Debug.WriteLine("Saved card settings");
            NotifyPropertyChanged("ActiveCard");
            NotifyPropertyChanged("Cards");
        }

        int _activePivotItem;
        public int ActivePivotItem
        {
            get
            {
                return _activePivotItem;
            }
            set
            {
                _activePivotItem = value;
                NotifyPropertyChanged("ActivePivotItem");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}