﻿using MitRejsekort.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MitRejsekort.ViewModels
{
    public class SettingsViewModel : INotifyPropertyChanged
    {
        public Account Account1 { get; set; }
        public Account Account2 { get; set; }
        public int? UpdateInterval
        {
            get
            {
                return GetSettingInt("UpdateInterval");
            }
            set
            {
                SetSetting("UpdateInterval", value);
                NotifyPropertyChanged("UpdateInterval");
                NotifyPropertyChanged("UpdateIntervalText");
            }
        }

        public string UpdateIntervalText
        {
            get
            {
                if (!App.ViewModel.FullVersion)
                {
                    return AppResources.TrailVersionExplanation;
                }
                if (UpdateInterval == null || UpdateInterval.Value == 0)
                {
                    return AppResources.Interval0;
                }

                else
                {
                    switch (UpdateInterval.Value)
                    {
                        case 1:
                            return AppResources.Interval1;
                        case 2:
                            return AppResources.Interval2;
                        case 3:
                            return AppResources.Interval3;
                        case 4:
                            return AppResources.Interval4;
                        case 5:
                            return AppResources.Interval5;
                        case 6:
                            return AppResources.Interval6;
                    }  
                }
                throw new Exception("Invalid update interval");
            }
        }

        public string AppVersion
        {
            get
            {
                return GetAppVersion();
            }
        }

        public SettingsViewModel()
        {
            Account1 = new Account(1);
            Account2 = new Account(2);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public static void SetSetting(string key, string value)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                if (value == null)
                {
                    IsolatedStorageSettings.ApplicationSettings.Remove(key);
                }
                else
                {
                    IsolatedStorageSettings.ApplicationSettings[key] = value;
                }

            }
            else
            {
                IsolatedStorageSettings.ApplicationSettings.Add(key, value);
            }
            IsolatedStorageSettings.ApplicationSettings.Save();
        }

        public static string GetSetting(string key)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                return IsolatedStorageSettings.ApplicationSettings[key] as string;
            }
            else
            {
                return null;
            }
        }

        public static void SetSetting(string key, int? value)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                if (value == null)
                {
                    IsolatedStorageSettings.ApplicationSettings.Remove(key);
                }
                else
                {
                    IsolatedStorageSettings.ApplicationSettings[key] = value;
                }

            }
            else
            {
                IsolatedStorageSettings.ApplicationSettings.Add(key, value);
            }
            IsolatedStorageSettings.ApplicationSettings.Save();
        }

        public static int? GetSettingInt(string key)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                return IsolatedStorageSettings.ApplicationSettings[key] as int?;
            }
            else
            {
                return null;
            }
        }

        public static string GetAppVersion()
        {
            var xmlReaderSettings = new XmlReaderSettings
            {
                XmlResolver = new XmlXapResolver()
            };

            using (var xmlReader = XmlReader.Create("WMAppManifest.xml", xmlReaderSettings))
            {
                xmlReader.ReadToDescendant("App");
                return xmlReader.GetAttribute("Version");
            }
        }
    }

    public class Account
    {
        public string Username 
        {
            get
            {
                
                return SettingsViewModel.GetSetting("Username" + accountNo);
                
            }
            set
            {
                
                SettingsViewModel.SetSetting("Username" + accountNo, value);
            }

        }
        public string Password
        {
            get
            {
                return SettingsViewModel.GetSetting("Password" + accountNo);
            }
            set
            {

                SettingsViewModel.SetSetting("Password" + accountNo, value);
            }
        }

        private readonly int accountNo;

        public Account (int accountNo)
        {
            this.accountNo = accountNo;
        }

    }

}
