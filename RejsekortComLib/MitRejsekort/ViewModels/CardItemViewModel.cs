﻿using MitRejsekort.Resources;
using MitRejsekort.Storage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MitRejsekort.ViewModels
{
    public class CardItemViewModel : INotifyPropertyChanged
    {
        private CardDataItem _Card;
        public CardDataItem Card
        {
            get 
            {
                return _Card;
            }
            set
            {
                _Card = value;
                NotifyPropertyChanged("Card");
            }
        }
        public string CardName
        {
            get
            {
                if (Card.CardName == null || Card.CardName.Length == 0)
                {
                    return Card.CardType.Replace("rejsekort", "").Replace("onligt", ".");
                }
                else
                {
                    return Card.CardName;
                }
            }
            set
            {
                Card.CardName = value;
                RejsekortTilesAndToasts.Tiles.UpdateTile(Card);
                NotifyPropertyChanged("CardName");
                
            }
        }

        public string InactiveCard
        {
            get
            {
                return Card.InactiveCard;
            }
            set
            {
                Card.InactiveCard = value;
                NotifyPropertyChanged("InactiveCard");
            }
        }

        public bool NeedsReload
        {
            get
            {
                return 50 > Card.Balance.ToFloat();
            }
        }

        public string LowBalanceLabel
        {
            get
            {
                return String.Format(AppResources.CardLowBalance, Card.LowBalanceWarningVal);
            }
        }

        public bool HasLowBalance
        {
            get
            {
                return Card.LowBalanceWarningVal > Card.Balance.ToFloat();
            }
        }

        public int LowBalanceWarningVal
        {
            get
            {
                return Card.LowBalanceWarningVal;
            }
            set
            {
                Card.LowBalanceWarningVal = value;
                NotifyPropertyChanged("LowBalanceLabel");
                NotifyPropertyChanged("HasLowBalance");
            }
        }

        public DateTime? LatestServerUpdate
        {
            get
            {
                return Card.LatestServerUpdate.FromUnixTime();
            }
        }

        public DateTime? LatestClientUpdate
        {
            get
            {
                return Card.LatestClientUpdate.FromUnixTime();
            }
        }

        public string ReloadText
        {
            get
            {
                if (Card.ManualReloadPending.Equals("Y"))
                {
                    return AppResources.ReloadPending;
                    
                } else if (Card.AutomaticReloadAmount != null && 
                    Card.AutomaticReloadAmount.ToFloat() > 0) {
                    return String.Format(AppResources.ReloadAutomatic,
                        Card.AutomaticReloadAmount.ToFloat(),
                        Card.AutomaticReloadAt.ToFloat());
                } else {
                    return AppResources.ReloadManuel;
                }
            }
        }

        public List<ActivityHistoryItem> ActivityHistory { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class PriceConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }
            return (value as int?) / 100f;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }
            return (value as float?) * 100;
        }
    }

    public class UnixTimeConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || (value as Int64?).Value == 0)
            {
                return null;
            }
            
            return (value as Int64?).FromUnixTime();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }
            return (value as DateTime?).ToUnixTime();
        }
    }

    public class YNConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            return (value as string).Equals("Y");
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return false;
            }
            return (value as bool?).Value ? "Y" : "N";
        }
    }
}
