﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MitRejsekort.Resources;
using RejsekortHttpComLib;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using MitRejsekort.Storage;
using Microsoft.Phone.Scheduler;
using MitRejsekort.ViewModels;
using System.Windows.Input;
using System.Windows.Data;
using System.Text;
using Microsoft.Phone.Tasks;
using WinPhoneExtensions;

namespace MitRejsekort
{
    public partial class MainPage : PhoneApplicationPage
    {
        private int adsDisableCounter = 0;
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            DataContext = App.ViewModel;

            BuildLocalizedApplicationBar();
        }

        // Load data for the ViewModel Items
        // This is loaded async in App.xaml.cs
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            GoogleAnalytics.EasyTracker.GetTracker().SendView("MainPage");
        }

        // Sample code for building a localized ApplicationBar
        private void BuildLocalizedApplicationBar()
        {
            // Set the page's ApplicationBar to a new instance of ApplicationBar.
            ApplicationBar = new ApplicationBar();

            // Create a new button and set the text value to the localized string from AppResources.
            ApplicationBarIconButton refreshButton = new ApplicationBarIconButton(
                new Uri("/Assets/AppBar/refresh.png", UriKind.Relative));
            refreshButton.Text = AppResources.Refresh;
            refreshButton.Click += refreshButton_Click;
            ApplicationBar.Buttons.Add(refreshButton);
            //ApplicationBar.Mode = ApplicationBarMode.Minimized;
        }

        async void refreshButton_Click(object sender, EventArgs e)
        {
            await refresh();
        }

        private async Task refresh()
        {
            ApplicationBar.IsVisible = false;

            var progress = new Progress<ComProgress>();

            progress.ProgressChanged += (object senderProgress, ComProgress comProgress) =>
            {
                Debug.WriteLine(comProgress);
                App.ViewModel.ComProgress = comProgress;
            };

            if (App.ViewModel.Settings.Account1.Username != null &&
                App.ViewModel.Settings.Account1.Password != null)
            {
                try
                {
                    var updater = new MitRejsekortUpdater.Updater(
                    App.ViewModel.Settings.Account1.Username,
                    App.ViewModel.Settings.Account1.Password, progress);
                    updater.CardUpdatedEvent += updater_CardUpdatedEvent;
                    await updater.Update(true).AsyncTrace();
                    // Update from db
                    await App.ViewModel.LoadCards().AsyncTrace();
                    
                    // Show cardlist
                    App.ViewModel.ActivePivotItem = 0;

                }
                catch (LoginException)
                {
                    MessageBox.Show(AppResources.LoginError, AppResources.ComError, MessageBoxButton.OK);
                }
                catch (LoginTokenException)
                {
                    MessageBox.Show(AppResources.ComNoConnection, AppResources.ComError, MessageBoxButton.OK);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.StackTrace);
                    //if (Debugger.IsAttached)
                    //{
                    //    Debugger.Break();
                    //}
                    var res = MessageBox.Show(AppResources.ComErrorSendReport, AppResources.ComError, MessageBoxButton.OKCancel);
                    if (res == MessageBoxResult.OK)
                    {
                        var email = new EmailComposeTask();
                        email.To = "support@jbr.dk";
                        email.Subject = "Error report";
                        email.Body = "Exception on manual refresh\n" + ex.Message + "\nStack trace:\n" + ex.FullTrace() +
                            "\n\nTrace html\n" + ex.Data["url"] + "\n" + ex.Data["html"] + "\n\nApp version: " + ViewModels.SettingsViewModel.GetAppVersion();
                        email.Show();
                    }
                }
            }
            ApplicationBar.IsVisible = true;
            App.ViewModel.ComProgress = null;
        }

        static void updater_CardUpdatedEvent(CardDataItem card, MitRejsekortUpdater.UpdatedData updatedEvent)
        {
            Debug.WriteLine("CardUpdatedEvent: {0}", updatedEvent);
            RejsekortTilesAndToasts.Toasts.CardUpdatedEvent(card, updatedEvent);
            RejsekortTilesAndToasts.Tiles.UpdateTile(card);
        }


        private void txtUsername_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.PlatformKeyCode == 0x0A)
            {
                txtPassword.Focus();
            }
        }

        private void grdCardLine_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.ViewModel.ActiveCard = (CardItemViewModel)lstCards.SelectedItem;
            NavigationService.Navigate(new Uri("/CardPage.xaml", UriKind.Relative));
        }

        private void txbUpdateIntervalText_DoubleTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (adsDisableCounter > 2)
            {
                var currentMargin = pvtMain.Margin;
                currentMargin.Bottom = 18;
                pvtMain.Margin = currentMargin;
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {           
            if (e.Key == Key.Enter || e.PlatformKeyCode == 0x0A)
            {
                sldrInterval.Focus();                
            }
        }

        async private void txtPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            await refresh();
        }

        private void txtPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            // Update binding on every keydown
            var expression = txtPassword.GetBindingExpression(PasswordBox.PasswordProperty);
            expression.UpdateSource();
        }

        private void mniPinToStart_Click(object sender, RoutedEventArgs e)
        {
            if (App.ViewModel.FullVersion)
            {
                var selectedCard = ((sender as MenuItem).DataContext as CardItemViewModel).Card;
                AddTile(selectedCard);
            }
            else
            {
                MessageBox.Show(AppResources.TrailVersionExplanation);
            }
        }

        /// <summary>
        /// Should have been in the RejsekortTilesAndToasts lib, but doing so violates some
        /// windows phone constraints in the agent project where Tile create is prohibited
        /// </summary>
        /// <param name="card"></param>
        public static void AddTile(CardDataItem card)
        {
            var tileData = RejsekortTilesAndToasts.Tiles.GetSecondaryTileData(card);
            var existingTile = RejsekortTilesAndToasts.Tiles.FindTile(card);
            if (existingTile != null)
            {
                existingTile.Delete();
            }
            ShellTile.Create(new Uri(RejsekortTilesAndToasts.Tiles.GetTileUri(card), UriKind.Relative), tileData, true);
        }
    }

    public class ProgressTextConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var stepName = value as ComProgressStepName?;
            switch(stepName.Value)
            {
                case ComProgressStepName.BASISDATA:
                    return AppResources.ProgressTextBasisData;
                case ComProgressStepName.CARDLIST:
                    return AppResources.ProgressTextCardList;
                case ComProgressStepName.HISTORY:
                    return AppResources.ProgressTextHistory;
                case ComProgressStepName.LOGIN:
                    return AppResources.ProgressTextLogin;
                case ComProgressStepName.DONE:
                    return AppResources.ProgressTextStoring;
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BalanceTextConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var balanceAsString = value as string;
            return balanceAsString.ToFloat();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class InactiveConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ("Y".Equals(value as string))
            {
                return 0.5d;
            }
            return 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class VisibilityConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (value as bool?).Value ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class VisibilityYNConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return "Y".Equals(value as string) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class VisibilityNYConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return "Y".Equals(value as string) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}