﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MitRejsekort.Storage
{
    /*
     * +-----------------------------------------------------------+
     * | CARDDATA                                                  |
     * +-----------------------+-----------------------------------+
     * | ID                    | PK                                |
     * | CARDHOLDER            | TEXT                              |
     * | LATESTCLIENTUPDATE    | INTEGER (EPOC secs)               |
     * | LATESTSERVERUPDATE    | INTEGER (EPOC secs)               |
     * | BALANCE               | TEXT (with . as decimalseperator) |
     * | DISCOUNTLEVELEAST     | TEXT                              |
     * | DISCOUNTLEVELWEST     | TEXT                              |
     * | DISCOUNTLEVELBELT     | TEXT                              |
     * | PUBLICCARDNO          | TEXT                              |
     * | CARDELECTRONICNO      | TEXT                              |
     * | CARDTYPE              | TEXT                              |
     * | CARDNAME              | TEXT (users own name)             |
     * | MANUALRELOADPENDING   | TEXT (Y/N)                        |
     * | AUTOMATICRELOADAMOUNT | TEXT (with . as decimalseperator) |
     * | AUTOMATICRELOADAT     | TEXT (with . as decimalseperator) |
     * | LOWBALANCEWARNINGVAL  | INTEGER                           |
     * | WARNLOWBALANCE        | TEXT (Y/N)                        |
     * | WARNRELOAD            | TEXT (Y/N)                        |
     * | WARNUPDATES           | TEXT (Y/N)                        |
     * | INACTIVECARD          | TEXT (Y/N)                        |
     * +-----------------------+-----------------------------------+
     */
    public class CardDataItem
    {
        public const string ID = "_id";
        public const string CARDHOLDER = "cardholder";
        public const string LATESTCLIENTUPDATE = "latestclientupdate";
        public const string LATESTSERVERUPDATE = "latestserverupdate";
        public const string BALANCE = "balance";
        public const string DISCOUNTLEVELEAST = "discountleveleast";
        public const string DISCOUNTLEVELWEST = "discountlevelwest";
        public const string DISCOUNTLEVELBELT = "discountlevelbelt";
        public const string PUBLICCARDNO = "publiccardno";
        public const string CARDELECTRONICNO = "cardelectronicno";
        public const string CARDTYPE = "cardtype";
        public const string CARDNAME = "cardname";
        public const string MANUALRELOADPENDING = "manualreloadpending";
        public const string AUTOMATICRELOADAMOUNT = "automaticreloadamount";
        public const string AUTOMATICRELOADAT = "automaticreloadat";
        public const string LOWBALANCEWARNINGVAL = "lowbalancewarningval";
        public const string WARNLOWBALANCE = "warnlowbalance";
        public const string WARNRELOAD = "warnreload";
        public const string WARNUPDATES = "warnupdates";
        public const string INACTIVECARD = "inactivecard";


 
        public int Id {get; set; }
        public string Cardholder { get; set; }
        public long? LatestClientUpdate { get; set; }
        public long? LatestServerUpdate { get; set; }
        public string Balance { get; set; }
        public string DiscountLevelEast { get; set; }
        public string DiscountLevelWest { get; set; }
        public string DiscountLevelBelt { get; set; }
        public string PublicCardNo { get; set; }
        public string CardElectronicNo { get; set; }
        public string CardType { get; set; }
        public string CardName { get; set; }
        public string CardNameCalculated
        {
            get
            {
                if (CardName == null || CardName.Length == 0)
                {
                    return CardType.Replace("rejsekort", "").Replace("onligt", ".");
                }
                else
                {
                    return CardName;
                }
            }
        }

        public string ManualReloadPending { get; set; }
        public string AutomaticReloadAmount { get; set; }
        public string AutomaticReloadAt { get; set; }
        public int LowBalanceWarningVal { get; set; }
        public string WarnLowBalance { get; set; }
        public string WarnReload { get; set; }
        public string WarnUpdates { get; set; }
        public string InactiveCard { get; set; }


    }
}
