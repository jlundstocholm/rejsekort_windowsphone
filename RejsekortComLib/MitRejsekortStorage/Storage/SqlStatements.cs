﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MitRejsekort.Storage
{
    class SqlStatements
    {
        public const string CARDDATA_TABLE_CREATE =
            "CREATE TABLE " + RejsekortStorage.CARDDATA_TABLE_NAME + " (" +
            CardDataItem.ID + " integer primary key autoincrement, " +
            CardDataItem.CARDHOLDER + " TEXT, " +
            CardDataItem.LATESTCLIENTUPDATE + " INTEGER, " +
            CardDataItem.LATESTSERVERUPDATE + " INTEGER, " +
            CardDataItem.BALANCE + " TEXT, " +
            CardDataItem.DISCOUNTLEVELEAST + " TEXT, " +
            CardDataItem.DISCOUNTLEVELWEST + " TEXT, " +
            CardDataItem.DISCOUNTLEVELBELT + " TEXT, " +
            CardDataItem.PUBLICCARDNO + " TEXT, " +
            CardDataItem.CARDELECTRONICNO + " TEXT, " +
            CardDataItem.CARDTYPE + " TEXT, " +
            CardDataItem.CARDNAME + " TEXT," +
            CardDataItem.MANUALRELOADPENDING + " TEXT," +
            CardDataItem.AUTOMATICRELOADAMOUNT + " TEXT," +
            CardDataItem.AUTOMATICRELOADAT + " TEXT," +
            CardDataItem.INACTIVECARD + " TEXT DEFAULT 'N'," +
            CardDataItem.LOWBALANCEWARNINGVAL + " INTEGER DEFAULT 0," +
            CardDataItem.WARNLOWBALANCE + " TEXT DEFAULT 'N', " +
            CardDataItem.WARNRELOAD + " TEXT DEFAULT 'N', " +
            CardDataItem.WARNUPDATES + " TEXT DEFAULT 'N');";

        public const string ACTIVITYHISTORY_TABLE_CREATE =
            "CREATE TABLE " + RejsekortStorage.ACTIVITYHISTORY_TABLE_NAME + " (" +
                    ActivityHistoryItem.ID + " integer primary key autoincrement, " +
                    ActivityHistoryItem.CARDDATAID + " INTEGER, " +
                    ActivityHistoryItem.STARTDATE + " INTEGER, " +
                    ActivityHistoryItem.STARTPLACE + " TEXT, " +
                    ActivityHistoryItem.ENDDATE + " INTEGER, " +
                    ActivityHistoryItem.ENDPLACE + " TEXT, " +
                    ActivityHistoryItem.PRICE + " INTEGER, " +
                    ActivityHistoryItem.ACTIVITYTYPE + " TEXT);";

        public const string SQL_DATABASE_VERSION =
            "pragma user_version;";

        public const string SQL_SET_DATABASE_VERSION =
            "pragma user_version = {0};";

        public const string SQL_GROUP_HEADERS =
            "select min(_id) as _id, sum(price) as amount, min(startdate) as startdate, date(startdate, 'unixepoch', 'localtime') as groupdate from activityhistory where carddataid = ? group by date(startdate, 'unixepoch', 'localtime') order by startdate desc;";

        public const string SQL_GROUP_MONTH_HEADERS =
                "select sum(price), strftime('%m %Y', date(startdate, 'unixepoch', 'localtime')) " +
                        "from activityhistory " +
                        "where price > 0 " +
                        "group by strftime('%m %Y', date(startdate, 'unixepoch', 'localtime'));";

        public const string SQL_GROUP_DAY_EXPENSES =
                "select sum(price), strftime('%Y%m-%d', date(startdate, 'unixepoch', 'localtime')) " +
                        "from activityhistory " +
                        "where price < 0 " +
                        "and carddataid = ? " +
                        "group by strftime('%Y%m-%d', date(startdate, 'unixepoch', 'localtime'));";

        public const string SQL_GROUP_WEEK_EXPENSES =
                "select sum(price), strftime('%Y-%W', date(startdate, 'unixepoch', 'localtime')) " +
                        "from activityhistory " +
                        "where price < 0 " +
                        "and carddataid = ? " +
                        "group by strftime('%Y-%W', date(startdate, 'unixepoch', 'localtime'));";

        public const string SQL_GROUP_MONTH_EXPENSES =
                "select sum(price), strftime('%Y-%m', date(startdate, 'unixepoch', 'localtime')) " +
                        "from activityhistory " +
                        "where price < 0 " +
                        "and carddataid = ? " +
                        "group by strftime('%Y-%m', date(startdate, 'unixepoch', 'localtime'));";

        public const string SQL_GROUP_WEEKDAY_EXPENSES =
                "select sum(price), strftime('%w', date(startdate, 'unixepoch', 'localtime')) " +
                        "from activityhistory " +
                        "where price < 0 " +
                        "and carddataid = ? " +
                        "group by strftime('%w', date(startdate, 'unixepoch', 'localtime'));";

        public const string SQL_GROUP_DAY_TIME =
                "select sum(enddate-startdate), strftime('%Y%m-%d', date(startdate, 'unixepoch', 'localtime')) " +
                        "from activityhistory " +
                        "where price < 0 " +
                        "and carddataid = ? " +
                        "group by strftime('%Y%m-%d', date(startdate, 'unixepoch', 'localtime'));";

        public const string SQL_GROUP_WEEK_TIME =
                "select sum(enddate-startdate), strftime('%Y-%W', date(startdate, 'unixepoch', 'localtime')) " +
                        "from activityhistory " +
                        "where price < 0 " +
                        "and carddataid = ? " +
                        "group by strftime('%Y-%W', date(startdate, 'unixepoch', 'localtime'));";

        public const string SQL_GROUP_MONTH_TIME =
                "select sum(enddate-startdate), strftime('%Y-%m', date(startdate, 'unixepoch', 'localtime')) " +
                        "from activityhistory " +
                        "where price < 0 " +
                        "and carddataid = ? " +
                        "group by strftime('%Y-%m', date(startdate, 'unixepoch', 'localtime'));";

        public const string SQL_GROUP_WEEKDAY_TIME =
                "select sum(enddate-startdate), strftime('%w', date(startdate, 'unixepoch', 'localtime')) " +
                        "from activityhistory " +
                        "where price < 0 " +
                        "and carddataid = ? " +
                        "group by strftime('%w', date(startdate, 'unixepoch', 'localtime'));";

        public const string SQL_GROUP_DAY_COUNT =
                "select count(*), strftime('%Y%m-%d', date(startdate, 'unixepoch', 'localtime')) " +
                        "from activityhistory " +
                        "where price < 0 " +
                        "and carddataid = ? " +
                        "group by strftime('%Y%m-%d', date(startdate, 'unixepoch', 'localtime'));";

        public const string SQL_GROUP_WEEK_COUNT =
                "select count(*), strftime('%Y-%W', date(startdate, 'unixepoch', 'localtime')) " +
                        "from activityhistory " +
                        "where price < 0 " +
                        "and carddataid = ? " +
                        "group by strftime('%Y-%W', date(startdate, 'unixepoch', 'localtime'));";

        public const string SQL_GROUP_MONTH_COUNT =
                "select count(*), strftime('%Y-%m', date(startdate, 'unixepoch', 'localtime')) " +
                        "from activityhistory " +
                        "where price < 0 " +
                        "and carddataid = ? " +
                        "group by strftime('%Y-%m', date(startdate, 'unixepoch', 'localtime'));";

        public const string SQL_GROUP_WEEKDAY_COUNT =
                "select count(*), strftime('%w', date(startdate, 'unixepoch', 'localtime')) " +
                        "from activityhistory " +
                        "where price < 0 " +
                        "and carddataid = ? " +
                        "group by strftime('%w', date(startdate, 'unixepoch', 'localtime'));";

        public const string SQL_STATS_FIRST_AND_LATEST =
                "select (select min(startdate) from activityhistory where carddataid = ?) as startdate, (select max(startdate) from activityhistory where carddataid = ?) as enddate;";

        public const string SQL_STATS_SUM_EXPENCES =
                "select sum(price) from activityhistory where price < 0 and carddataid = ?;";

        public const string SQL_STATS_SUM_EXPENCES_LATEST_30_DAYS =
                "select sum(price) from activityhistory where price < 0 and carddataid = ? and date(startdate, 'unixepoch', 'localtime') > date('now','-1 month');";

        public const string SQL_STATS_COUNT_EXPENCES =
                "select count(price) from activityhistory where price < 0 and carddataid = ?;";

        public const string SQL_STATS_COUNT_EXPENCES_LATEST_30_DAYS =
                "select count(price) from activityhistory where price < 0 and carddataid = ? and date(startdate, 'unixepoch', 'localtime') >= date('now','-1 month');";

        public const string SQL_STATS_COUNT_CHECKOUT_MISSING =
                "select count(price) from activityhistory where endplace = 'Check ud mangler' and carddataid = ?;";

        public const string SQL_STATS_COUNT_CHECKOUT_MISSING_FOR_FACEBOOK =
                "select count(price) from activityhistory where endplace = 'Check ud mangler' and carddataid = ? and date(startdate, 'unixepoch', 'localtime') <= date('now','-1 day');";

        public const string SQL_STATS_LATEST_CHECKOUT_MISSING =
                "select max(startdate) from activityhistory where endplace = 'Check ud mangler' and carddataid = ?;";

        public const string SQL_STATS_SUM_TIMESPENT_INALL =
                "select sum(enddate-startdate) from activityhistory where enddate is not null and carddataid = ?;";

        public const string SQL_STATS_LATEST_RELOAD =
                "select startdate from activityhistory where price > 0 and carddataid = ? order by startdate desc";

        public const string SQL_UPDATE_ACTIVITYTYPES =
                "update activityhistory set activitytype = 'MESSAGE' where enddate is null and carddataid = ?;";
        public const string SQL_INSERT_ACTIVITYHISTORY_ITEM =
            "insert into " + RejsekortStorage.ACTIVITYHISTORY_TABLE_NAME + " (" +
            ActivityHistoryItem.CARDDATAID + ", " +
            ActivityHistoryItem.STARTDATE + ", " +
            ActivityHistoryItem.STARTPLACE + ", " +
            ActivityHistoryItem.ENDDATE + ", " +
            ActivityHistoryItem.ENDPLACE + ", " +
            ActivityHistoryItem.PRICE + ", " +
            ActivityHistoryItem.ACTIVITYTYPE +
            ") values (" + 
            "@" + ActivityHistoryItem.CARDDATAID + ", " +
            "@" + ActivityHistoryItem.STARTDATE + ", " +
            "@" + ActivityHistoryItem.STARTPLACE + ", " +
            "@" + ActivityHistoryItem.ENDDATE + ", " +
            "@" + ActivityHistoryItem.ENDPLACE + ", " +
            "@" + ActivityHistoryItem.PRICE + ", " +
            "@" + ActivityHistoryItem.ACTIVITYTYPE +
            ")";

        public const string SQL_FIND_ACTIVITYHISTORY_ITEM_BY_CARDDATAID =
            "select * from " + RejsekortStorage.ACTIVITYHISTORY_TABLE_NAME + 
            " where " + ActivityHistoryItem.CARDDATAID + " = @" + ActivityHistoryItem.CARDDATAID + 
            " order by " + ActivityHistoryItem.STARTDATE + " DESC;";

        public const string SQL_DELETE_ACTIVITYHISTORY_ITEMS_BY_STARTDATE =
            "delete from " + RejsekortStorage.ACTIVITYHISTORY_TABLE_NAME +
            " where " + ActivityHistoryItem.CARDDATAID + " = @" + ActivityHistoryItem.CARDDATAID +
            " and " + ActivityHistoryItem.STARTDATE + " >= @" + ActivityHistoryItem.STARTDATE + ";";

        public const string SQL_FIND_ALL_CARDDATA_ITEMS =
            "select * " +
            " from " + RejsekortStorage.CARDDATA_TABLE_NAME +  
            " order by " + CardDataItem.INACTIVECARD + ", " + CardDataItem.CARDNAME + ", " + CardDataItem.PUBLICCARDNO + ";";

        public const string SQL_FIND_CARDDATA_BY_ID =
            "select * from " + RejsekortStorage.CARDDATA_TABLE_NAME +
            " where _id = @" + CardDataItem.ID + ";";

        public const string SQL_FIND_CARDDATA_BY_PUBLICCARDNO =
            "select * from " + RejsekortStorage.CARDDATA_TABLE_NAME +
            " where " + CardDataItem.PUBLICCARDNO + " = @" + CardDataItem.PUBLICCARDNO + ";";

        public const string SQL_UPDATE_CARDDATA_CARDNAME_AND_STUFF =
            "update " + RejsekortStorage.CARDDATA_TABLE_NAME + 
            " set " +
            CardDataItem.CARDNAME + " = @" + CardDataItem.CARDNAME + ", " +
            CardDataItem.INACTIVECARD + " = @" + CardDataItem.INACTIVECARD + ", " +
            CardDataItem.LOWBALANCEWARNINGVAL + " = @" + CardDataItem.LOWBALANCEWARNINGVAL + ", " +
            CardDataItem.WARNLOWBALANCE + " = @" + CardDataItem.WARNLOWBALANCE + ", " +
            CardDataItem.WARNRELOAD + " = @" + CardDataItem.WARNRELOAD + ", " +
            CardDataItem.WARNUPDATES + " = @" + CardDataItem.WARNUPDATES +
            " where " + CardDataItem.PUBLICCARDNO + " = @" + CardDataItem.PUBLICCARDNO;

        public const string SQL_INSERT_OR_REPLACE_CARDDATA =
            "insert or replace into " + RejsekortStorage.CARDDATA_TABLE_NAME + " (" +
            CardDataItem.ID + ", " +
            CardDataItem.CARDHOLDER + ", " +
            CardDataItem.LATESTCLIENTUPDATE + ", " +
            CardDataItem.LATESTSERVERUPDATE + ", " +
            CardDataItem.BALANCE + ", " +
            CardDataItem.DISCOUNTLEVELEAST + ", " +
            CardDataItem.DISCOUNTLEVELWEST + ", " +
            CardDataItem.DISCOUNTLEVELBELT + ", " +
            CardDataItem.PUBLICCARDNO + ", " +
            CardDataItem.CARDELECTRONICNO + ", " +
            CardDataItem.CARDTYPE + ", " +
            CardDataItem.CARDNAME + ", " +
            CardDataItem.MANUALRELOADPENDING + ", " +
            CardDataItem.AUTOMATICRELOADAMOUNT + ", " +
            CardDataItem.AUTOMATICRELOADAT + ", " +
            CardDataItem.INACTIVECARD + ", " +
            CardDataItem.LOWBALANCEWARNINGVAL + ", " +
            CardDataItem.WARNLOWBALANCE + ", " +
            CardDataItem.WARNRELOAD + ", " +
            CardDataItem.WARNUPDATES + ") values (" +
            "@" + CardDataItem.ID + ", " +
            "@" + CardDataItem.CARDHOLDER + ", " +
            "@" + CardDataItem.LATESTCLIENTUPDATE + ", " +
            "@" + CardDataItem.LATESTSERVERUPDATE + ", " +
            "@" + CardDataItem.BALANCE + ", " +
            "@" + CardDataItem.DISCOUNTLEVELEAST + ", " +
            "@" + CardDataItem.DISCOUNTLEVELWEST + ", " +
            "@" + CardDataItem.DISCOUNTLEVELBELT + ", " +
            "@" + CardDataItem.PUBLICCARDNO + ", " +
            "@" + CardDataItem.CARDELECTRONICNO + ", " +
            "@" + CardDataItem.CARDTYPE + ", " +
            "@" + CardDataItem.CARDNAME + ", " +
            "@" + CardDataItem.MANUALRELOADPENDING + ", " +
            "@" + CardDataItem.AUTOMATICRELOADAMOUNT + ", " +
            "@" + CardDataItem.AUTOMATICRELOADAT + ", " +
            "@" + CardDataItem.INACTIVECARD + ", " +
            "@" + CardDataItem.LOWBALANCEWARNINGVAL + ", " +
            "@" + CardDataItem.WARNLOWBALANCE + ", " +
            "@" + CardDataItem.WARNRELOAD + ", " +
            "@" + CardDataItem.WARNUPDATES + "); ";
            
    }
}
