﻿using SQLiteWinRT;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace MitRejsekort.Storage
{
    public class RejsekortStorage
    {
        private Database database;
        private static RejsekortStorage instance;
        public const string CARDDATA_TABLE_NAME = "CARDDATA";
        public const int APP_DATABASE_VERSION = 5;
        public const string ACTIVITYHISTORY_TABLE_NAME = "ACTIVITYHISTORY";

        private RejsekortStorage()
        {
            database = new Database(ApplicationData.Current.LocalFolder, "rejsekort.db");
        }

        public async Task OpenDatabase()
        {
            await database.OpenAsync();
            var databaseVersion = await DatabaseVersion();
            if (databaseVersion == 0)
            {
                // First run. Just create the tables. No upgrades
                await CreateDatabase();
            }
            else if (databaseVersion < APP_DATABASE_VERSION)
            {
                UpgradeDatabase(databaseVersion, APP_DATABASE_VERSION);
            }
        }

        private void UpgradeDatabase(int fromVersion, int toVersion)
        {
            // Nothing to upgrade yet
        }

        public static async Task<RejsekortStorage> Instance()
        {
            if (instance == null)
            {
                instance = new RejsekortStorage();
                await instance.OpenDatabase();
            }
            return instance;
        }

        public async Task<int> DatabaseVersion()
        {
            using (var statement = await database.PrepareStatementAsync(SqlStatements.SQL_DATABASE_VERSION))
            {
                if (await statement.StepAsync())
                {
                    var version = statement.GetIntAt(0);
                    Debug.WriteLine("Got database version to {0}", version);
                    return version;
                }
            }
            throw new Exception("Could not get database version in sqlite!");
        }

        private async Task SetDatabaseVersion(int version)
        {
            await database.ExecuteStatementAsync(String.Format(SqlStatements.SQL_SET_DATABASE_VERSION, version));
            Debug.WriteLine("Changed database version to {0}", version);
            
        }

        public async Task CreateDatabase()
        {
            Debug.WriteLine("Creating tables...");
            await database.ExecuteStatementAsync(SqlStatements.ACTIVITYHISTORY_TABLE_CREATE); 
            await database.ExecuteStatementAsync(SqlStatements.CARDDATA_TABLE_CREATE);
            await SetDatabaseVersion(APP_DATABASE_VERSION);
            Debug.WriteLine("Done creating tables");
        }

        public async Task AddActivityHistoryItem(ActivityHistoryItem item)
        {
            using (var statement = await database.PrepareStatementAsync(SqlStatements.SQL_INSERT_ACTIVITYHISTORY_ITEM))
            {
                statement.BindInt(ActivityHistoryItem.CARDDATAID, item.CardDataId);
                statement.BindText(ActivityHistoryItem.STARTPLACE, item.StartPlace);
                statement.BindInt64(ActivityHistoryItem.STARTDATE, item.StartDate);
                statement.BindText(ActivityHistoryItem.ENDPLACE, item.EndPlace);
                statement.BindInt64(ActivityHistoryItem.ENDDATE, item.EndDate);
                statement.BindText(ActivityHistoryItem.ACTIVITYTYPE, item.ActivityType);
                statement.BindInt(ActivityHistoryItem.PRICE, item.Price);
                await statement.StepAsync();
                Debug.WriteLine("Added new Activity History to database: StartPlace:{0}, StartDate:{1}, EndPlace:{2}, EndDate:{3}, ActivityType:{4}",
                    item.StartPlace, item.StartDate, item.EndPlace, item.EndDate, item.ActivityType);
            }
        }

        public async Task<List<ActivityHistoryItem>> GetActivitityHistoryItems(int carddataid)
        {
            var items = new List<ActivityHistoryItem>();
            using (var statement = await database.PrepareStatementAsync(SqlStatements.SQL_FIND_ACTIVITYHISTORY_ITEM_BY_CARDDATAID))
            {
                statement.BindInt(ActivityHistoryItem.CARDDATAID, carddataid);
                while (await statement.StepAsync())
                {
                    var item = new ActivityHistoryItem()
                    {
                        CardDataId = statement.GetInt(ActivityHistoryItem.CARDDATAID),
                        ActivityType = statement.GetText(ActivityHistoryItem.ACTIVITYTYPE),
                        Price = statement.GetInt(ActivityHistoryItem.PRICE),
                        StartPlace = statement.GetText(ActivityHistoryItem.STARTPLACE),
                        StartDate = statement.GetInt64(ActivityHistoryItem.STARTDATE),
                        EndPlace = statement.GetText(ActivityHistoryItem.ENDPLACE),
                        EndDate = statement.GetInt64(ActivityHistoryItem.ENDDATE)
                    };
                    items.Add(item);
                }
            }
            return items;
        }

        public async Task DeleteActivityHistoryItems(int carddataid, long? startdate)
        {
            using (var statement = await database.PrepareStatementAsync(SqlStatements.SQL_DELETE_ACTIVITYHISTORY_ITEMS_BY_STARTDATE))
            {
                statement.BindInt(ActivityHistoryItem.CARDDATAID, carddataid);
                statement.BindInt64(ActivityHistoryItem.STARTDATE, startdate.HasValue ? startdate.Value : 0);
                await statement.StepAsync();
                Debug.WriteLine("Deleted all historyitems for carddataid {0} from {1} and onwards",
                    carddataid, startdate);
            }
        }

        /**
         * Get all cards
         */
        public async Task<List<CardDataItem>> Cards()
        {
            var cards = new List<CardDataItem>();
            var sql = SqlStatements.SQL_FIND_ALL_CARDDATA_ITEMS;
            using (var statement = await database.PrepareStatementAsync(sql))
            {
                statement.EnableColumnsProperty();
                while (await statement.StepAsync())
                {
                    var card = ToCardDataItem(statement);
                    cards.Add(card);
                }
                
            }
            Debug.WriteLine("Loaded {0} cards", cards.Count());
            return cards;
        }

        public async Task<CardDataItem> CardById(int id)
        {
            using (var statement = await database.PrepareStatementAsync(SqlStatements.SQL_FIND_CARDDATA_BY_ID))
            {
                statement.BindInt(CardDataItem.ID, id);
                if (await statement.StepAsync())
                {
                    return ToCardDataItem(statement);
                }
            }
            return null;
        }

        public async Task<CardDataItem> CardByPublicCardNo(string publicCardNo)
        {
            using (var statement = await database.PrepareStatementAsync(SqlStatements.SQL_FIND_CARDDATA_BY_PUBLICCARDNO))
            {
                statement.BindText(CardDataItem.PUBLICCARDNO, publicCardNo);
                if (await statement.StepAsync())
                {
                    return ToCardDataItem(statement);
                }
            }
            return null;
        }

        private static CardDataItem ToCardDataItem(Statement statement)
        {
            var card = new CardDataItem()
            {
                AutomaticReloadAmount = statement.GetText(CardDataItem.AUTOMATICRELOADAMOUNT),
                AutomaticReloadAt = statement.GetText(CardDataItem.AUTOMATICRELOADAT),
                Balance = statement.GetText(CardDataItem.BALANCE),
                CardElectronicNo = statement.GetText(CardDataItem.CARDELECTRONICNO),
                Cardholder = statement.GetText(CardDataItem.CARDHOLDER),
                CardName = statement.GetText(CardDataItem.CARDNAME),
                CardType = statement.GetText(CardDataItem.CARDTYPE),
                DiscountLevelBelt = statement.GetText(CardDataItem.DISCOUNTLEVELBELT),
                DiscountLevelEast = statement.GetText(CardDataItem.DISCOUNTLEVELEAST),
                DiscountLevelWest = statement.GetText(CardDataItem.DISCOUNTLEVELWEST),
                Id = statement.GetInt(CardDataItem.ID),
                InactiveCard = statement.GetText(CardDataItem.INACTIVECARD),
                LatestClientUpdate = statement.GetInt64(CardDataItem.LATESTCLIENTUPDATE),
                LatestServerUpdate = statement.GetInt64(CardDataItem.LATESTSERVERUPDATE),
                LowBalanceWarningVal = statement.GetInt(CardDataItem.LOWBALANCEWARNINGVAL),
                ManualReloadPending = statement.GetText(CardDataItem.MANUALRELOADPENDING),
                PublicCardNo = statement.GetText(CardDataItem.PUBLICCARDNO),
                WarnLowBalance = statement.GetText(CardDataItem.WARNLOWBALANCE),
                WarnReload = statement.GetText(CardDataItem.WARNRELOAD),
                WarnUpdates = statement.GetText(CardDataItem.WARNUPDATES)
                
            };
            return card;
        }
        public async Task UpdateCardNameAndStuff(CardDataItem item, string cardName,
                                           int lowBalance, bool inactiveCard,
                                           bool warnLowBalance, bool warnReload,
                                           bool warnUpdates)
        {
            using (var statement = await database.PrepareStatementAsync(SqlStatements.SQL_UPDATE_CARDDATA_CARDNAME_AND_STUFF))
            {
                statement.BindText(CardDataItem.CARDNAME, cardName);
                statement.BindInt(CardDataItem.LOWBALANCEWARNINGVAL, lowBalance);
                statement.BindText(CardDataItem.INACTIVECARD, inactiveCard ? "Y" : "N");
                statement.BindText(CardDataItem.WARNLOWBALANCE, warnLowBalance ? "Y" : "N");
                statement.BindText(CardDataItem.WARNRELOAD, warnReload ? "Y" : "N");
                statement.BindText(CardDataItem.WARNUPDATES, warnUpdates ? "Y" : "N");
                statement.BindText(CardDataItem.PUBLICCARDNO, item.PublicCardNo);
                await statement.StepAsync();
                Debug.WriteLine("Updated card and stuff");
            }
        }

        /**
         * Returns a tuple where first element is the new CardDataItem, and 2nd element is the old item (if any)
         */
        public async Task<Tuple<CardDataItem, CardDataItem>> CreateOrUpdateCardDataItem(CardDataItem item)
        {
            var existing = await CardByPublicCardNo(item.PublicCardNo);
            var sql = SqlStatements.SQL_INSERT_OR_REPLACE_CARDDATA;
            using (var statement = await database.PrepareStatementAsync(sql))
            {
                if (existing != null)
                {
                    statement.BindInt64(CardDataItem.ID, existing.Id);
                    //Since the whole row is replaced we need to set
                    //data for the fields that needs to survive the replace
                    statement.BindText(CardDataItem.CARDNAME, existing.CardName);
                    statement.BindInt(CardDataItem.LOWBALANCEWARNINGVAL, existing.LowBalanceWarningVal);
                    statement.BindText(CardDataItem.INACTIVECARD, existing.InactiveCard);
                    statement.BindText(CardDataItem.WARNLOWBALANCE, existing.WarnLowBalance);
                    statement.BindText(CardDataItem.WARNRELOAD, existing.WarnReload);
                    statement.BindText(CardDataItem.WARNUPDATES, existing.WarnUpdates);
                }
                else
                {
                    statement.BindInt64(CardDataItem.ID, null);
                }
                statement.BindText(CardDataItem.AUTOMATICRELOADAMOUNT, item.AutomaticReloadAmount);
                statement.BindText(CardDataItem.AUTOMATICRELOADAT, item.AutomaticReloadAt);
                statement.BindText(CardDataItem.BALANCE, item.Balance);
                statement.BindText(CardDataItem.CARDELECTRONICNO, item.CardElectronicNo);
                statement.BindText(CardDataItem.CARDHOLDER, item.Cardholder);
                statement.BindText(CardDataItem.CARDTYPE, item.CardType);
                statement.BindText(CardDataItem.DISCOUNTLEVELBELT, item.DiscountLevelBelt);
                statement.BindText(CardDataItem.DISCOUNTLEVELEAST, item.DiscountLevelEast);
                statement.BindText(CardDataItem.DISCOUNTLEVELWEST, item.DiscountLevelWest);
                statement.BindInt64(CardDataItem.LATESTCLIENTUPDATE, item.LatestClientUpdate);
                statement.BindInt64(CardDataItem.LATESTSERVERUPDATE, item.LatestServerUpdate);
                statement.BindText(CardDataItem.MANUALRELOADPENDING, item.ManualReloadPending);
                statement.BindText(CardDataItem.PUBLICCARDNO, item.PublicCardNo);
                await statement.StepAsync();
                Debug.WriteLine("Updated or inserted carddataitem {0}", item);
                var newitem = await CardByPublicCardNo(item.PublicCardNo);
                return Tuple.Create(newitem, existing);
            }
        }
        
    }

    public static class RejsekortStorageExtionsions 
    {
        public static string ToDbString(this float value)
        {
            return value.ToString(System.Globalization.NumberFormatInfo.InvariantInfo);
        }

        public static float ToFloat(this string value)
        {
            return float.Parse(value,
                    System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.AllowLeadingSign,
                    System.Globalization.NumberFormatInfo.InvariantInfo);
        }

        public static DateTime? FromUnixTime(this long? unixTime)
        {
            if (unixTime == null)
            {
                return null;
            }
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime.Value);
        }

        public static long? ToUnixTime(this DateTime? date)
        {
            if (!date.HasValue)
            {
                return null;
            }
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date.Value - epoch).TotalSeconds);
        }

        public static void BindText(this Statement statement, string parameter, string value)
        {
            if (value == null)
            {
                statement.BindNullParameterWithName("@" + parameter);
            }
            else
            {
                statement.BindTextParameterWithName("@" + parameter, value);
            }
        }
        public static void BindInt(this Statement statement, string parameter, Nullable<int> value)
        {
            if (value.HasValue)
            {
                statement.BindIntParameterWithName("@" + parameter, value.Value);
            }
            else
            {
                statement.BindNullParameterWithName("@" + parameter);
            }
        }

        public static void BindInt64(this Statement statement, string parameter, Nullable<Int64> value)
        {
            if (value.HasValue)
            {
                statement.BindInt64ParameterWithName("@" + parameter, value.Value);
            }
            else
            {
                statement.BindNullParameterWithName("@" + parameter);
            }
        }

        public static string GetText(this Statement statement, string columnName)
        {
            for (int i = 0; i < statement.ColumnCount; i++)
            {
                if (statement.GetColumnName(i).Equals(columnName))
                {
                    return statement.GetTextAt(i);
                }
            }
            throw new Exception("Column " + columnName + " not found");
        }

        public static int GetInt(this Statement statement, string columnName)
        {
            for (int i = 0; i < statement.ColumnCount; i++)
            {
                if (statement.GetColumnName(i).Equals(columnName))
                {
                    return statement.GetIntAt(i);
                }
            }
            throw new Exception("Column " + columnName + " not found");
        }

        public static Int64 GetInt64(this Statement statement, string columnName)
        {
            for (int i = 0; i < statement.ColumnCount; i++)
            {
                if (statement.GetColumnName(i).Equals(columnName))
                {
                    return statement.GetInt64At(i);
                }
            }
            throw new Exception("Column " + columnName + " not found");
        }
    }

}
