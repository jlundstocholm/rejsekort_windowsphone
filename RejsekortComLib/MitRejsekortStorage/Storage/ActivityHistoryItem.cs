﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MitRejsekort.Storage
{
    public class ActivityHistoryItem
    {
        public const string ID = "_id";
        public const string STARTDATE = "startdate";
        public const string STARTPLACE = "startplace";
        public const string ENDDATE = "enddate";
        public const string ENDPLACE = "endplace";
        public const string PRICE = "price";
        public const string ACTIVITYTYPE = "activitytype";
        public const string CARDDATAID = "carddataid";

        public int Id { get; set; }
        public int CardDataId { get; set; }
        public long? StartDate { get; set; }
        public string StartPlace { get; set; }
        public long? EndDate { get; set; }
        public string EndPlace { get; set; }
        public int Price { get; set; }
        public string ActivityType { get; set; }
    }
}
