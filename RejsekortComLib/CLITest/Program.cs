﻿using System;
using RejsekortHttpComLib;

namespace CLITest
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var c = new Com();
            var progress = new Progress<ComProgress>();
            progress.ProgressChanged += (object sender, ComProgress e) => {
                Console.WriteLine("Progress update: {0}", e);
            };
            var cardListTask = c.FetchData("jaybora", "Lotte1601", progress, ServerCom.HistoryPeriod.ThreeMonths);
            Console.WriteLine("Awaiting task to finish...");
            cardListTask.Wait();
            var cards = cardListTask.Result;
        }
    }
}
