﻿using MitRejsekort.Storage;
using RejsekortHttpComLib;
using System;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using WinPhoneExtensions;

namespace MitRejsekortUpdater
{
    public enum UpdatedData
    {
        REFRESH,
        NEW_DATA,
        RELOAD,
        LOW_BALANCE
    }
    public delegate void CardUpdatedEventHandler(CardDataItem card, UpdatedData updatedEvent);
    public class Updater
    {
        private readonly string username;
        private readonly string password;
        private IProgress<RejsekortHttpComLib.ComProgress> progress;
        public event CardUpdatedEventHandler CardUpdatedEvent;

        public Updater(string username, string password, IProgress<RejsekortHttpComLib.ComProgress> progress)
        {
            this.username = username;
            this.password = password;
            this.progress = progress;
            CardUpdatedEvent += Updater_CardUpdatedEvent;
        }

        void Updater_CardUpdatedEvent(CardDataItem card, UpdatedData updatedEvent)
        {
            Debug.WriteLine("Update Event fired: {0}", updatedEvent);
        }

        public async Task Update(Boolean fullHistory)
        {
            var rejsekortCom = new RejsekortHttpComLib.Com();
            
            var cards = await rejsekortCom.FetchData(username, password, progress, 
                fullHistory ? ServerCom.HistoryPeriod.ThreeMonths: ServerCom.HistoryPeriod.TwoWeeks).AsyncTrace();

            foreach (var card in cards)
            {
                var item = new CardDataItem()
                {
                    AutomaticReloadAmount = card.AutomaticReloadAmount.ToDbString(),
                    AutomaticReloadAt = card.AutomaticReloadAt.ToDbString(),
                    Balance = card.Balance.ToDbString(),
                    CardElectronicNo = card.ElectronicCardNo,
                    Cardholder = card.CardHolder,
                    CardType = card.CardType,
                    DiscountLevelBelt = card.DiscountLevelGreatBelt.ToString(),
                    DiscountLevelEast = card.DiscountLevelEast.ToString(),
                    DiscountLevelWest = card.DiscountLevelWest.ToString(),
                    LatestClientUpdate = card.LatestClientUpdate.ToUnixTime(),
                    LatestServerUpdate = card.LatestServerUpdate.ToUnixTime(),
                    ManualReloadPending = card.ManualReloadPending ? "Y" : "N",
                    PublicCardNo = card.PublicCardNo
                };
                var storage = await RejsekortStorage.Instance();
                var newAndOldCardDataItem = await storage.CreateOrUpdateCardDataItem(item);
                var cardData = await storage.CardByPublicCardNo(item.PublicCardNo);

                if (card.Travels.Count > 0 &&
                    card.Travels[card.Travels.Count-1].StartPlace.EventDateTime.HasValue)
                {
                    await storage.DeleteActivityHistoryItems(cardData.Id, card.Travels[card.Travels.Count-1].StartPlace.EventDateTime.ToUnixTime());
                }

                foreach (var travel in card.Travels)
                {
                    var historyItem = new ActivityHistoryItem()
                    {
                        CardDataId = cardData.Id,
                        Price = travel.Price,
                        ActivityType = travel.EndPlace.EventDateTime != null
                            ? "TRAVEL" : "MESSAGE"

                    };
                    if (travel.StartPlace != null && travel.StartPlace.EventDateTime != null)
                    {
                        historyItem.StartDate = travel.StartPlace.EventDateTime.ToUnixTime();
                    }
                    if (travel.StartPlace != null)
                    {
                        historyItem.StartPlace = travel.StartPlace.Name;
                    }
                    if (travel.EndPlace != null && travel.EndPlace.EventDateTime != null)
                    {
                        historyItem.EndDate = travel.EndPlace.EventDateTime.ToUnixTime();
                    }
                    if (travel.EndPlace != null)
                    {
                        historyItem.EndPlace = travel.EndPlace.Name;
                    }

                    await storage.AddActivityHistoryItem(historyItem);
                }
                if (CardUpdatedEvent != null && newAndOldCardDataItem.Item1 != null)
                {
                    CardUpdatedEvent(newAndOldCardDataItem.Item1, UpdatedData.REFRESH);
                    // Check for new data, ie the latest card activity date has been updated
                    if (newAndOldCardDataItem.Item2 != null &&
                        newAndOldCardDataItem.Item1.LatestServerUpdate != newAndOldCardDataItem.Item2.LatestServerUpdate)
                    {
                        CardUpdatedEvent(newAndOldCardDataItem.Item1, UpdatedData.NEW_DATA);
                        if (newAndOldCardDataItem.Item1.Balance.ToFloat() < newAndOldCardDataItem.Item1.LowBalanceWarningVal)
                        {
                            CardUpdatedEvent(newAndOldCardDataItem.Item1, UpdatedData.LOW_BALANCE);
                        }
                        if (newAndOldCardDataItem.Item2 != null &&
                            newAndOldCardDataItem.Item1.Balance.ToFloat() >
                            newAndOldCardDataItem.Item2.Balance.ToFloat())
                        {
                            CardUpdatedEvent(newAndOldCardDataItem.Item1, UpdatedData.RELOAD);
                        }
                    }
                }
            }
            SetSetting("LastUpdate", DateTime.Now);
        }

        public static void SetSetting(string key, DateTime? value)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
            {
                if (value == null)
                {
                    IsolatedStorageSettings.ApplicationSettings.Remove(key);
                }
                else
                {
                    IsolatedStorageSettings.ApplicationSettings[key] = value;
                }

            }
            else
            {
                IsolatedStorageSettings.ApplicationSettings.Add(key, value);
            }
            IsolatedStorageSettings.ApplicationSettings.Save();
        }
    }
}